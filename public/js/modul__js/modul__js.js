
function all_ajax(route, data, code, success) {
    $.ajax({
        type: 'post',
        url: route,
        data: data,
        success: function (data) {
            $('body').loadingModal('destroy');
            if (data.code == code) {
                success(data);
            } else {
                alert(data.message, 'error');
            }

        }, error: function () {
            alert("Lỗi hệ thống!", "error");
        }
    });
};

function create_form_ajax(route, data, code, success) {
    $.ajax({
        type: 'post',
        url: route,
        async: false,
        processData: false,
        contentType: false,
        data: data,
        success: function (data) {
            if (data.code == code) {
                success(data);
            } else {
                alert(data.message, 'error');
            }
        }, error: function () {
            alert("Lỗi hệ thống!", "error");
        }
    });
}

function update_form_ajax(route, data, code, success) {
    $.ajax({
        type: 'post',
        url: route,
        async: false,
        processData: false,
        contentType: false,
        data: data,
        success: function (data) {
            if (data.code == code) {
                success(data);
            } else {
                alert(data.message, 'error');
            }
        }, error: function () {
            $('body').loadingModal('destroy');
            alert("Lỗi hệ thống!", "error");
        }
    });
}
function update_status_check(id,type,route){
    if($('.show-sale-'+id).prop('checked')){
        var value=1;
    }else{
        var value=2;
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {value: value,id:id,type:type},
        url: route,
        success: function (data) {
            $('.show-sale-'+id).css('display','inline');
            $('.img-show-sale-'+id).css('display','none');
        },beforeSend:function(){
            $('.show-sale-'+id).css('display','none');
            $('.img-show-sale-'+id).css('display','block');
        }
    });
}
