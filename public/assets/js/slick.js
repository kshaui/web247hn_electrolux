$('.inspiredRelated-list').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: false,
  dots: false,
  centerMode: false,
  focusOnSelect: false
});

// slider ảnh chi tiết sản phẩm 
$('.slider-productTop').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-productBottom'
});
$('.slider-productBottom').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slider-productTop',
  dots: false,
  centerMode: false,
  focusOnSelect: true
});

$('.slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  dots: true,
  autoplaySpeed: 3000,
});

$('.categoryHome-list').slick({
  slidesToShow: 8,
  slidesToScroll: 1,
  autoplay: false,
  dots: false,
  autoplaySpeed: 3000,
  focusOnSelect: true,
  prevArrow:'<button class="slick-prev"> < </button>',
  nextArrow:'<button class="slick-next"> > </button>',
});