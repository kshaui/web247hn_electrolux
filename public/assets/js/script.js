function isEmail(email) {
 var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 return regex.test(email);
}

function isPhone(phone) {
  var regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
  return regex.test(phone);
}

// hiệu ứng ajax
function load_box() {
  $('#loading_box').css({"opacity":"1"});
  $('#loading_box').css({"visibility":"inherit"});  
}

function end_load_box() {
  $('#loading_box').css({"opacity":"0"});
  $('#loading_box').css({"visibility":"hidden"}); 
}

// js chạy thanh list menu
(function($) {
  var $main_nav = $('#main-nav');
  var $toggle = $('.toggle');

  var defaultData = {
    maxWidth: false,
    customToggle: $toggle,
    navTitle: '',
    levelTitles: true,
    pushContent: '#container',
    insertClose: 2,
    closeLevels: false
  };

  $main_nav.find('li.add').children('a').on('click', function() {
    var $this = $(this);
    var $li = $this.parent();
    var items = eval('(' + $this.attr('data-add') + ')');

    $li.before('<li class="new"><a>'+items[0]+'</a></li>');

    items.shift();

    if (!items.length) {
      $li.remove();
    }
    else {
      $this.attr('data-add', JSON.stringify(items));
    }

    Nav.update(true);
  });

  var Nav = $main_nav.hcOffcanvasNav(defaultData);

  const update = (settings) => {
    if (Nav.isOpen()) {
      Nav.on('close.once', function() {
        Nav.update(settings);
        Nav.open();
      });

      Nav.close();
    }
    else {
      Nav.update(settings);
    }
  };

  $('.actions').find('a').on('click', function(e) {
    e.preventDefault();

    var $this = $(this).addClass('active');
    var $siblings = $this.parent().siblings().children('a').removeClass('active');
    var settings = eval('(' + $this.data('demo') + ')');

    update(settings);
  });

  $('.actions').find('input').on('change', function() {
    var $this = $(this);
    var settings = eval('(' + $this.data('demo') + ')');

    if ($this.is(':checked')) {
      update(settings);
    }
    else {
      var removeData = {};
      $.each(settings, function(index, value) {
        removeData[index] = false;
      });

      update(removeData);
    }
  });
})(jQuery);

// Khi người dùng cuộn xuống 20px từ đầu
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    $('.header_bottom').css({"top":"-100px"});
    $('.header_bottom').css({"top":"0"});
    $('.header_bottom').css({"position":"fixed"});
    $('.header_bottom').css({"background":"#fff"});
  } else {
    $('.header_bottom').css({"position":"inherit"});
    $('.header_bottom').css({"top":"-100px"});
  }
}

// $('.categoryHome').slick({
//   slidesToShow: 5,
//   slidesToScroll: 1,
//   autoplay: false,
//   dots: false,
// });

$(".btn-sendcmt").click(function() {
  $(".form").slideToggle();
});

$(function(){
  var show = false;
  $("#view-detail").click(function(){
    if(show==false){
      $(".categoryProduct-detail").css({"height":"unset"});
      show=true;
      $("#view-detail").text("Ẩn bớt");
    }else{
      $(".categoryProduct-detail").css({"height":"310px"});
      $(document).scrollTop("1230");
      show=false;
      $("#view-detail").text("Xem thêm giới thiệu");
    }
  })
})

$(function(){
  var show = false;
  $("#btn-viewadd").click(function(){
    if(show==false){
      $(".detailProduct-introduce").css({"height":"unset"});
      show=true;
      $("#btn-viewadd").text("Ẩn bớt");
    }else{
      $(".detailProduct-introduce").css({"height":"310px"});
      $(document).scrollTop("830");
      show=false;
      $("#btn-viewadd").text("Xem thêm giới thiệu");
    }
  })
})

// Cộng trừ sản phẩm 
// Cập nhật qty trong chi tiết sản phẩm
var CartPlusMinus = $('.detailProduct-info__qty');
$(".qtybutton").on("click", function() {
  var $button = $(this);
  var oldValue = $button.parent().find("input").val();
  if ($button.text() === "+") {
    var newVal = parseFloat(oldValue) + 1;
  } else {
// Don't allow decrementing below zero
if (oldValue > 0) {
  var newVal = parseFloat(oldValue) - 1;
} else {
  newVal = 1;
}
}
$button.parent().find("input").val(newVal);
});

// js nút active button danh mục trang danh mục tin tức
$(".btn-home").each(function(){
    $(this).click(function(){
        $(".btn-home").removeClass("active");
        $(this).addClass("active");
        $data_form = $(this).data('form');
        $data_hidden = $(this).data('hidden');
        $($data_form).slideDown();
        $($data_hidden).slideUp();
    })
});


// đoạn js này cần dùng foreach để đổ và add id vào sau support-btnform và support-form 
// cho phần tìm loại sản phẩm trang hỗ trợ

$("#support-btnform").each(function(){
    $(this).click(function(){
        $("#support-form").slideToggle();
        $("#support-form2").slideUp();
        $("#support-form3").slideUp();
    })
});

$("#support-btnform1").each(function(){
    $(this).click(function(){
        $("#support-form1").slideToggle();
        $("#support-form").slideUp();
        $("#support-form3").slideUp();
    })
});

$("#support-btnform2").each(function(){
    $(this).click(function(){
        $("#support-form2").slideToggle();
        $("#support-form").slideUp();
        $("#support-form1").slideUp();
    })
});
// kết thúc

// đoạn js này cần dùng foreach để đổ và add id vào inspiration-btn và inspiration-form 
// cho phần danh mục ở khơi nguồn cảm hứng
$("#inspiration-btn1").each(function(){
    $(this).click(function(){
        $("#inspiration-form1").slideDown();
        $("#inspiration-form2").slideUp();
        $("#inspiration-form3").slideUp();
        $("#inspiration-form4").slideUp();
        $("#inspiration-btn1").addClass("active");
        $("#inspiration-btn2").removeClass("active");
        $("#inspiration-btn3").removeClass("active");
        $("#inspiration-btn4").removeClass("active");
    })
});
$("#inspiration-btn2").each(function(){
    $(this).click(function(){
        $("#inspiration-form2").slideDown();
        $("#inspiration-form1").slideUp();
        $("#inspiration-form3").slideUp();
        $("#inspiration-form4").slideUp();
        $("#inspiration-btn2").addClass("active");
        $("#inspiration-btn1").removeClass("active");
        $("#inspiration-btn3").removeClass("active");
        $("#inspiration-btn4").removeClass("active");
    })
});
$("#inspiration-btn3").each(function(){
    $(this).click(function(){
        $("#inspiration-form3").slideDown();
        $("#inspiration-form2").slideUp();
        $("#inspiration-form1").slideUp();
        $("#inspiration-form4").slideUp();
        $("#inspiration-btn3").addClass("active");
        $("#inspiration-btn2").removeClass("active");
        $("#inspiration-btn1").removeClass("active");
        $("#inspiration-btn4").removeClass("active");
    })
});
$("#inspiration-btn4").each(function(){
    $(this).click(function(){
        $("#inspiration-form4").slideDown();
        $("#inspiration-form2").slideUp();
        $("#inspiration-form3").slideUp();
        $("#inspiration-form1").slideUp();
        $("#inspiration-btn4").addClass("active");
        $("#inspiration-btn2").removeClass("active");
        $("#inspiration-btn3").removeClass("active");
        $("#inspiration-btn1").removeClass("active");
    })
});
// kết thúc


// đoạn js này cần dùng foreach để đổ và add id vào support-questionBtn và support-questionform 
// cho phần câu hỏi thường gặp trang hỗ trợ
$("#support-questionBtn1").each(function(){
    $(this).click(function(){
        $("#support-questionform1").slideToggle();
        $("#support-questionform2").slideUp();
        $("#support-questionform3").slideUp();
    })
});

$("#support-questionBtn2").each(function(){
    $(this).click(function(){
        $("#support-questionform2").slideToggle();
        $("#support-questionform1").slideUp();
        $("#support-questionform3").slideUp();
    })
});

$("#support-questionBtn3").each(function(){
    $(this).click(function(){
        $("#support-questionform3").slideToggle();
        $("#support-questionform2").slideUp();
        $("#support-questionform1").slideUp();
    })
});

// kết thúc



// xóa sản phẩm khỏi giỏ hàng
function remove_cart(rowId){
  if (confirm('Bạn có muốn xóa sản phẩm khỏi giỏ hàng không??')) {
    var token = $('input[name="_token"]').val();
    $.ajax({
      type: 'post',
      url:'/ajax/xoa-san-pham',
      data:{rowId:rowId,_token: token},
      success: function(data) {
        $('#delete-cart-'+rowId).remove();
        $('#total').html(data.count);
        $('#count').html(data.count_cart);
        $('#price').html(data.price);
        $('#total').html(data.price);
        console.log(data);
        if (data.count_update==0) {
          location.reload();
        }else{

        }
      }
    });
  }
}

// nút cộng sản phẩm trang giỏ hàng
$('#order .cart-btnPlus').on('click', function(e){
  var token = $('input[name="_token"]').val();
  var rowId = $(this).attr('data-update');
  var type = 1;
  var number = $(this).data('soluong');
  e = $(this);
  $.ajax({
    type: 'post',
    dataType: 'json',
    data: {rowId:rowId,type:type, _token:token,},
    url: '/ajax/edit-cart',
    success:function(data){
      $('#count').html(data.count_cart);
      $('#counts').html(data.count_cart);
      e.parent('td').children('.cart-show').val(data.count_product);
      e.parent('td').children('.cart-btnMinus').data('soluong',data.count_product);
      $('#total_all').html(data.count);
      $('#total').html(data.count);
      $('#price').html(data.count);
    }
  });
});

// load quận huyện
$('#province_id').on('change', function(){
  var province_id = $(this).val();
  var token = $('input[name="_token"]').val();
  $.ajax({
    type: 'post',
    dataType: 'json',
    data: {"_token":token,province_id : province_id},
    url: '/ajax/dia-chi',
    success:function(data){
      $('#district_id').html(data.district);
    }
  });
});

// cập nhật số lượng sản phẩm trừ 
$('#order .cart-btnMinus').on('click', function(){
  var token = $('input[name="_token"]').val();
  var rowId = $(this).attr('data-update');
  var type = -1;
  // đếm số lượng sản phẩm
  var number = $(this).data('soluong');
  e = $(this);
    //nếu số lượng sản phẩm lớn hơn 1 cho xóa bình thường
    if(number > 1){
      $.ajax({
        type: 'post',
        dataType: 'json',
        data: {rowId:rowId, type:type ,_token:token},
        url: '/ajax/edit-cart',
        success:function(data){
          $('#count').html(data.count_cart);
          $('#counts').html(data.count_cart);
          e.parent('td').children('.cart-show').val(data.count_product);
          e.parent('td').children('.cart-btnMinus').data('soluong',data.count_product);
          $('#total').html(data.count);
          $('#price').html(data.count);
          $('#total_all').html(data.count);
          console.log(data);
        }
      });
    }else{
      // nếu số lượng sản phẩm bằng 1 thì hỏi có muốn xóa sản phẩm không
      if (confirm('Bạn có muốn xóa sản phẩm khỏi giỏ hàng không?')) {
        $.ajax({
          type: 'post',
          url:'/ajax/xoa-san-pham',
          data:{rowId:rowId,_token:token},
          success: function(data) {
            $('#delete-cart-'+rowId).remove();
            $('#total').html(data.count);
            $('#totals').html(data.count);
            $('#count').html(data.count_cart);
            $('#price').html(data.price);
            console.log(data);
            if (data.count_update==0) {
              location.reload();
            }else{

            }
          }
        });
      }
    }
});

// validate sdt
function validatePhone(){  
  var regex =/((09|03|07|08|05)+([0-9]{8})\b)/g;
  return regex.test(phone);
}
//validate email
function validateEmail(){
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
//đặt hàng
$(document).ready(function(){
  $('body').on('click','#putCart',function(e) {
    e.preventDefault();
    name = $('#create_cart input[name=name]').val();
    phone = $('#create_cart input[name=phone]').val();
    adress = $('#create_cart input[name=adress]').val();
    email = $('#create_cart input[name=email]').val();
    provincial = $('#create_cart select[name=provincial]').val();
    district = $('#create_cart select[name=district]').val();


    if (name == '' && phone == '' && email == '' && adress == '' && provincial == '' && district == '') {
      alert('Vui lòng nhập đầy đủ thông tin!');
    }else if(name == '') {
      alert('Họ và tên không được để rỗng!');
    }else if (phone == '') {
      alert('Số điện thoại không được để rỗng!');
    }else if (!validatePhone(phone)) {
      alert('Định dạng điện thoại không chính xác!');
    }else if (adress == '') {
      alert('Địa chỉ giao hàng không được để rỗng!');
    }else if (email == '') {
      alert('Email không được để rỗng!');
    }else if (!validateEmail(email)) {
      alert('Định dạng Email không chính xác!');
    }else if (provincial == "") {
      alert('Vui lòng chọn tỉnh / thành phố nhận hàng !');
    }else if (district == "") {
      alert('Vui lòng chọn quận / huyện nhận hàng !');
    }else {
      $.ajax({
        type: 'post',
        url: '/ajax/luu-don-hang',
        async: false,
        processData: false,
        contentType: false,
        data: new FormData($('#create_cart')[0]),
        success: function(data) {
          $('input[name=name]').val('');
          $('textarea[name=email]').val('');
          $('input[name=adress]').val('');
          $('input[name=phone]').val('');
          $('input[name=total_price]').val('');
          $('select[name=provincial]').val('');
          $('select[name=district]').val('');
          alert("Đặt hàng thành công");
          console.log(data);
          window.location = '/dat-hang-thanh-cong';
        },
        error: function() {
          alert("Lỗi hệ thống !", "error");
        }
      });
    }
  });
});

/**
 * Thêm vào giỏi hàng
 */
$('.add_to_cart').on("click",function(event){
  event.preventDefault();
  var id = $(this).data('id');
  var type = $(this).data('type');
  var qty = 1;
  var token = $(this).data('token');
  var link =  window.location.href;
  $.ajax({
      type:"post",
      dataType:"json",
      url:"/ajax/dat-hang",
      data:{
          _token: token,
          id:id,
          type:type,
          qty :qty,
          link:link,
      },
      success:function(response){
          $('img.loading1').css('display','none');
          $('body').css({'opacity':1});
          // $("#fancy_data").find(".layer_cart_img img").attr("src",response.image);
          // $("#fancy_data").find(".product_name").html(response.name);
          // $("#fancy_data").find(".ajax_cart_quantity").html(response.cart_count);
          // $("#fancy_data").find(".ajax_block_cart_total").html(response.cart_total);
          // $("#fancy_data").find(".layer_cart_product_quantity").html(response.qty);
          // $("#fancy_data").find(".layer_cart_product_price").html(response.price);
          // $(".cart .total_cart").html(response.cart_count);
          $('#countCart-header').text(response.cart_count);
          alert('Thêm sản phẩm vào giỏ hàng thành công');
      },
      error: function (error) {
          $('img.loading1').css('display','none');
          $('body').css({'opacity':1});
          alert('Có lỗi xảy ra! Vui lòng thử lại.');
      }, beforeSend:function(){
          $('img.loading1').css('display','inline');
          $('body').css({'opacity':0.5});
      }
  });
});

$('.buy_now').on("click",function(event){
  event.preventDefault();
  var id = $(this).data('id');
  var type = $(this).data('type');
  var qty = 1;
  var token = $(this).data('token');
  var link =  window.location.href;
  $.ajax({
      type:"post",
      dataType:"json",
      url:"/ajax/dat-hang",
      data:{
          _token: token,
          id:id,
          type:type,
          qty :qty,
          link:link,
      },
      success:function(response){
          $('img.loading1').css('display','none');
          $('body').css({'opacity':1});
          $("#fancy_data").find(".layer_cart_img img").attr("src",response.image);
          $("#fancy_data").find(".product_name").html(response.name);
          $("#fancy_data").find(".ajax_cart_quantity").html(response.cart_count);
          $("#fancy_data").find(".ajax_block_cart_total").html(response.cart_total);
          $("#fancy_data").find(".layer_cart_product_quantity").html(response.qty);
          $("#fancy_data").find(".layer_cart_product_price").html(response.price);
          $(".cart .total_cart").html(response.cart_count);
          window.location.href = "/gio-hang";
      },
      error: function (error) {
          $('img.loading1').css('display','none');
          $('body').css({'opacity':1});
          alert('Có lỗi xảy ra! Vui lòng thử lại.');
      }, beforeSend:function(){
          $('img.loading1').css('display','inline');
          $('body').css({'opacity':0.5});
      }
  });
});

$('#form-filter input').on('change', function(){
  $('#form-filter').submit();
});

function send_contact(){
  var name = $('.name_contacts_home').val();
  var email = $('.email_contacts_home').val();
  var phone = $('.phone_contacts_home').val();
  var token = $("input[name='_token']").val(); 

  if(name == ''){
    alert('Vui lòng nhập tên!');
  }else if(email == ''){
    alert('Vui lòng nhập email!');
  }else if(!isEmail(email)){
    alert('Email không đúng định dạng!');
  }else if(phone == ''){
    alert('Vui lòng nhập số điện thoại!')
  }else if(!isPhone(phone)){
    alert('Định dạng điện thoại không chính xác!')
  }else{
    load_box();
    $.ajax({
      type: 'post',
      dataType: 'json',
      data: {"_token":token,phone:phone,email:email, name:name},
      url: '/ajax/send-contacts',
      success:function(data){
        end_load_box()
        $status = data.data;
        if($status == -1){
          alert('Đã có lỗi xảy ra');
          $('#loading_box').css('visibility','hidden');
          $('#loading_box').css('opacity','0');
        }else if($status==1){
          $('.name_contacts_home').val('');
          $('.phone_contacts_home').val('');
          $('.email_contacts_home').val('');
          alert('Gửi yêu cầu thành công');
        }
      },
    });
  }
}

$(document).ready(function(){
  $('body').on('click','#putComment',function(e) {
    content = $(this).closest('form').find('textarea[name=content]').val();
    name = $(this).closest('form').find('input[name=name]').val();
    phone = $(this).closest('form').find('input[name=phone]').val();
    email = $(this).closest('form').find('input[name=email]').val();
    var token = $('input[name="_token"]').val();
    console.log(content)
    if (content == '' && name == '' && phone == '' && email == '') {
      alert('Vui lòng nhập đầy đủ thông tin!');
    }else if(content == '') {
      alert('Nội dung bình luận không được để rỗng!');
    }else if(name == '') {
      alert('Họ và tên không được để rỗng!');
    }else if (phone == '') {
      alert('Số điện thoại không được để rỗng!');
    }else if (!validatePhone(phone)) {
      alert('Định dạng điện thoại không chính xác!');
    }else if (email == '') {
      alert('Email không được để rỗng!');
    }else if (!validateEmail(email)) {
      alert('Định dạng Email không chính xác!');
    }else {
      load_box();
      $.ajax({
      type: 'post',
      url: '/binh-luan',
      async: false,
      processData: false,
      contentType: false,
      data: new FormData($(this).closest('form')[0]),
      beforeSend: function() {},
      success: function(data) {
        $('input[name=name]').val('');
        $('textarea[name=content]').val('');
        $('input[name=email]').val('');
        $('input[name=phone]').val('');
        $('radio[name=gender]').val('');
        end_load_box();
        alert("Cảm ơn bạn đã để lại góp ý");
      },
      error: function() {
        end_load_box();
        alert("Bình luận không thành công!", "error");
      }
    });
  }
  });
});