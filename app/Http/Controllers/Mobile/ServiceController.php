<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Service;
use App\News;
class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Dịch vụ',
            'description'=> 'Dịch vụ',
        ]);
        $breadcrumbs = [
            ['name'=> 'dịch vụ','url' => route('mobile.services.index')],               
        ];
        
        // lấy ra các tin được ghim cho trang dịch vụ
        $news = News::where('status',1)
        ->leftJoin('pins', 'news.id','=','pins.type_id')
        ->where('pins.type','news')
        ->where('pins.place','service')
        ->orderBy('value', 'asc')
        ->limit(3)
        ->get();

        if (count($news) < 0) {
            $news = News::where('status',1)->orderBy('created_at', 'asc')->limit(3)->get();
        }

        return view('mobile.services.index', compact('breadcrumbs','meta_seo','services','news'));
    }
    public function show($slug)
    {
        $service = Service::where('status',1)->where('slug',$slug)->firstOrFail();
        $news = News::where('status',1)->orderBy('created_at','desc')->limit(3)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => $service->getName(),
            'description'=> $service->getDes(120),
        ]);
        $breadcrumbs = [
             ['name'=> 'dịch vụ','url' => route('mobile.services.index')], 
            ['name'=> $service->getName(),'url' => '#'],               
        ];
       
        return view('mobile.services.show', compact('breadcrumbs','meta_seo','service','news'));
    }
}
