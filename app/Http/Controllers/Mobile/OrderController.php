<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;

// use Request;
use Cart;
use App\Order;
use App\OrderDetail;

use DB;
use View;
use App\MyClass\Categories;
use App\Product;
use App\District;
use App\Province;
use App\Comment;
use App\ProductCategory;
class OrderController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function orderSucces(Request $request) {
        $category_all = ProductCategory::where('status',1)->get();
        $config_general = DB::table('options')->select('value')->where('name','general')->first();

        if($config_general) {
            $config_general = json_decode(base64_decode($config_general->value),true);
            View::share('config_general',$config_general);
        }
        return view('mobile.order.success',compact('category_all','config_general'));
    }

    public function buyNow(Request $request){
    	$product = Product::where('status',1)->where('id',$request->product_id)->first();

    	$category_all = ProductCategory::where('status',1)->where('parent_id',0)->get();
    	Cart::add(['id' => $request->product_id,'name'=>$product->name??"",'price'=>$product->price,
    		'weight'=>25,'qty'=>$request->qty,'options' => ['image'=>$product->image,'promotion'=> $product->promotion??""]]);
        

    	$result['status'] = 1;
    	$result['html'] = '';
    	$result['count'] = Cart::count();
    	$result['price'] = Cart::total().' VNĐ';
    	$count = Cart::count();
    	$price = Cart::total();
    	$cart = Cart::content();
    	$result['html'] = view('web.layouts.header', compact('count','price','product','category_all'))->render();
    	return response()->json($result);
    }
    public function showCart(Request $request){
        $category_all = ProductCategory::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Giỏ hàng',
            'description'=> 'Mô tả giỏ hàng',
            'url' => url('/'),
            'image' => url('/').'/assets/img/logo.png'
        ]);
        return view('mobile.cart.index',compact('meta_seo','category_all'));
    }
    public function cartAdd(Request $request){
        $category_all = ProductCategory::where('status',1)->get();
        $province = Province::get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Đặt hàng',
            'description'=> 'Mô tả đặt hàng',
            'url' => url('/'),
            'image' => url('/').'/assets/img/logo.png'
        ]);
        return view('mobile.order.order',compact('meta_seo','category_all','province'));
    }
    public function updateQty(Request $request){
    	$rowId=$request->rowId;
    	$type=$request->type;
    	$cart = Cart::get($rowId);
    	$qty = $cart->qty + $type;
    	Cart::update($rowId, $qty);
    	$cart = Cart::get($rowId);
    	$result['status'] = 1;
    	$result['count_cart'] = Cart::count();
    	$result['count_product'] = Cart::get($rowId)->qty;
    	$result['count'] = Cart::total().' VNĐ';
    	$result['total_price'] = number_format($cart->price*$cart->qty).' VNĐ';
    	$result['value_total'] = Cart::subtotal(0,',').' VNĐ';
    	return json_encode($result);
    }
	
}
