<?php
namespace App\Http\Controllers\Mobile;
use Illuminate\Http\Request;
use App\Product;
use App\News;
class SearchController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $search        = str_replace("\'","'",$search);
        $search        = str_replace("'","''",$search);

        $breadcrumbs = [
            ['name'=> 'Tìm kiếm','url' => '/tim-kiem'],         
        ];

        $products = Product::where('status',1)->where('name','like','%'.$search.'%')->limit(16)->get();

        $news = News::join('news_categories','news_categories.id','news.category_id')
                    ->where('news.name', 'LIKE', "%$search%")
                    ->where('news.category_id','!=',0)
                    ->where('news.status',1)
                    // ->where('news_categories.status',1)
                    ->select('news.*')
                    ->take(10)->get();  
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Tìm kiếm'.' - Elextrolux',
            'description'=> 'Tìm kiếm',
        ]);
        return view('mobile.search.index', compact('news','meta_seo','products','breadcrumbs','search'));
    }
}
