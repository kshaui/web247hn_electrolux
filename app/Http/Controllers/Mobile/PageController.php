<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;

use App\Page;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::where('status',1)->where('slug',$slug)->firstOrFail();
        $meta_seo = $this->meta_seo('',0,[
            'title' => $page->getName(),
            'description'=> $page->getDes(),
        ]);
        $breadcrumbs = [
            ['name'=> $page->getName(),'url' => '#'],               
        ];
       
        return view('mobile.pages.index', compact('meta_seo','breadcrumbs','page'));
    }

}
