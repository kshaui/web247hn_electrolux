<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;
use App\Product;
class NewsController extends Controller
{
    public function AllShow()
    {
        $data = News::where('status',1)->paginate(10);
        $news_categories = NewsCategory::where('status',1)->where('parent_id','0')->get();

        $news_categories_child_collect =collect(  NewsCategory::where('status',1)->whereIn('parent_id',$news_categories->pluck('id','id')->toArray())->get() );
        $meta_seo = $this->meta_seo('',0,
        [
            'title' => 'Tin tức',
            'description'=> 'Tin tức ',
            'url' => url('').'/tin-tuc',
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],         
        ];
        return view('mobile.news.index',compact('data','meta_seo','breadcrumbs','news_categories','news_categories_child_collect'));
    }
    public function index($slug = null)
    {
        $category = NewsCategory::where('status',1)->where('slug',$slug)->firstOrFail();

        $news_categories = NewsCategory::where('status',1)->where('parent_id','0')->get();

        $news_categories_child_collect =collect(  NewsCategory::where('status',1)->whereIn('parent_id',$news_categories->pluck('id','id')->toArray())->get() );

        //nó
        // dump($category);
        // //cha nó
        // dump($category->getParent());
        // //các con nó
        // dump($category->getChilds());
        
        $child_ids = $category->getChildIds();
        $data = News::where('status',1)->whereIn('category_id',$child_ids)->paginate(20);
        // dd($data);

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $category->getName(),
            'description'=> $category->getDes(160),
            'url' => $category->getUrlMb(),
            'image'=> $category->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],   
            ['name'=> $category->getName(),'url' =>  $category->getUrlMb()],         
        ];

        return view('mobile.news.index',compact('data','meta_seo','breadcrumbs','news_categories','news_categories_child_collect'));
        
    }
    public function show($slug)
    {
        $news = News::where('status',1)->where('slug',$slug)->firstOrFail();
        $news_category = NewsCategory::where('status',1)
            ->where('id',$news->category_id)
            ->firstOrFail();

        $related_news = News::where('status',1)
            ->where('category_id',$news->category_id)
            ->get();

        $hot_products = Product::where('status',1)
            ->leftJoin('pins', 'products.id','=','pins.type_id')
            ->where('pins.type','products')
            ->where('pins.place','highlight')
            ->orderBy('value', 'asc')
            ->select('products.*')
            ->limit(20)->get();

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $news->getName(),
            'description'=> $news->getDes(160),
            'url' => $news->getUrlMb(),
            'image'=> $news->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Tin tức','url' => '/tin-tuc'],   
            ['name'=> $news_category->getName(),'url' =>  $news_category->getUrlMb()],         
        ];

        return view('mobile.news.show',compact('news','meta_seo','breadcrumbs','news_category','related_news','hot_products'));
    }
}
