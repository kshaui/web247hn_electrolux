<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use Mail;
use App\Mail\TestEmail;

class TestController extends Controller
{

    public function index()
    {
        //sending a test email
        $email = 'tranthanh.ceo96@gmail.com';
        Mail::to($email)->send(new TestEmail(['email'=>$email,'name'=>'web247hn-'.config('app.name')]));
    	die;
    }
}
