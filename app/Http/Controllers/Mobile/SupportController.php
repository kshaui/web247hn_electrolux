<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Service;
use App\News;
use App\ProductCategory;

class SupportController extends Controller
{
    public function index()
    {
        
        $breadcrumbs = [
            ['name'=> 'Hỗ trợ','url' => route('support.index')],               
        ];
        // lấy ra các loại sản phẩm là danh mục cha
        $cate = ProductCategory::where('status',1)->where('parent_id',0)->get();
        $cate_child = ProductCategory::where('status',1)->where('parent_id','!=',0)->get();

        return view('mobile.support.index', compact('breadcrumbs','services','news','cate','cate_child'));
    }

    public function show($slug)
    {
        $service = Service::where('status',1)->where('slug',$slug)->firstOrFail();
        $meta_seo = $this->meta_seo('',0,[
            'title' => $service->getName(),
            'description'=> $service->getDes(120),
        ]);
        $breadcrumbs = [
             ['name'=> 'dịch vụ','url' => route('web.services.index')], 
            ['name'=> $service->getName(),'url' => '#'],               
        ];
       
        return view('mobile.services.show', compact('breadcrumbs','service'));
    }
}
