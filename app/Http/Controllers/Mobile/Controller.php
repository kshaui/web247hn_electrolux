<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

use DB;
use View;
use Illuminate\Support\Facades\Cache;
class Controller extends BaseController
{
    public function __construct()
    {
        //constructor cho web
        $variable = 'variable';
        View::share('variable',$variable);
        
        Cache::flush();

        if(cache::has('menu_footer')) {
            $menu_footer = Cache::get('menu_footer');
        }else {
            $menu_footer = $this->ChildrenCategoryOption('general', 'menu_footer');
            Cache::forever('menu_footer', $menu_footer);
             
        }
      

        $config_general = DB::table('options')->select('value')->where('name','general')->first();
        if($config_general) {
            $config_general = json_decode(base64_decode($config_general->value),true);
            View::share('config_general',$config_general);
        }

        $config_home = $this->getOptions('home');

        // dd($config_home['conf_service']);
        View::share('menu_footer', $menu_footer);
        View::share('config_home', $config_home);
        // dd($config_home);

    }

   
    public function meta_seo($type='',$id=0,$options) {
        $meta_seo = [];
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }
    public function ChildrenCategoryOption($setting, $data)
    {
        $category = DB::table('options')->select('value')->where('name', $setting)->first();
        $category = json_decode(base64_decode($category->value), true);
        $data = json_decode($category[$data]);
        foreach ($data as $value) {
            if (!empty($value->children)) {
                $children = $value->children;
                $value->children = $children;
            }
        }
        return $data;
    }
     public function getOptions($name) {
        $cache_name = 'options_'.$name;
        if(Cache::has($cache_name)) {
            return Cache::get($cache_name);
        }else {
            $config = DB::table('options')->select('value')->where('name',$name)->first();
            if($config) {
                $config = json_decode(base64_decode($config->value),true);
                Cache::forever($cache_name,$config);
                return $config;
            }
        }
        return;
    }
}
