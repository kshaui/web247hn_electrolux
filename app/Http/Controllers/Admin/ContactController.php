<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
class ContactController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Liên hệ';
        $this->table_name = 'contacts';
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');


        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên','string',1);
        $listdata->add('email','Email','string',0);
        $listdata->add('phone','Số điện thoại','string',0);
        $listdata->add('content','Nội dung','string',0);
        $listdata->add('','Thông tin');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        //$listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
