<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 *
 * Phần cấu hình của core chúng ta chia thành 2 phần chính để tiện cho tối ưu query ngoài frontend:
 * - Cấu hình trang chủ: Seo (title, description, ...) của trang chủ, các nội dung chỉ hiện thị ở trang chủ như banner, các nội dung cố định ...
 * - Cấu hình chung: cho các nội dung chung trên toàn bộ các trang như: html head, hotline, link ở header, các thông tin ở footer ...
 *
 */

class SettingController extends Controller
{
    private function postData($setting_name, $data){
        unset($data['_token']);
        unset($data['submit']);
        $data = base64_encode(json_encode($data));
        if(DB::table('options')->where('name',$setting_name)->exists()){
            DB::table('options')->where('name',$setting_name)->update(['value'=>$data]);
        }else{
            DB::table('options')->insert(['name'=>$setting_name,'value'=>$data]);
        }
    }
    public function home(Request $request) {
        $setting_name = 'home';
        $title = "Cấu hình trang chủ";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->text('meta_title',isset($data['meta_title']) ? $data['meta_title'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description',isset($data['meta_description']) ? $data['meta_description'] : '',0,'Meta description','');
        $data_form[] = $form->text('hotline',isset($data['hotline']) ? $data['hotline'] : '',0,'Hotline');

        $data_form[] = $form->title('Cấu hình bộ sưu tập hiển thị trang chủ');
        $data_form[] = $form->text('collection_title',isset($data['collection_title']) ? $data['collection_title'] : '',0,'Tiêu đề bộ sưu tập');
        $data_form[] = $form->image('collection_img',isset($data['collection_img']) ? $data['collection_img'] : '',0,'Hình ảnh','');
        $data_form[] = $form->textarea('collection_iframe',isset($data['collection_iframe']) ? $data['collection_iframe'] : '',0,'Đường dẫn video','');


        $data_form[] = $form->title('Cấu hình dòng sản phẩm cao cấp  hiển thị trang chủ');
        $data_form[] = $form->text('advance_product_title',isset($data['advance_product_title']) ? $data['advance_product_title'] : '',0,'Tiêu đề sản phẩm cao cấp');
        $data_form[] = $form->text('advance_product_link',isset($data['advance_product_link']) ? $data['advance_product_link'] : '',0,'Url');
        $data_form[] = $form->image('advance_product_img',isset($data['advance_product_img']) ? $data['advance_product_img'] : '',0,'Hình ảnh','');
        $data_form[] = $form->textarea('advance_product_des',isset($data['advance_product_des']) ? $data['advance_product_des'] : '',0,'Mô tả','');

        $data_form[] = $form->title('Cấu hình tin tức có video hiển thị trang chủ');
         $data_form[] = $form->image('news_home_img',isset($data['news_home_img']) ? $data['news_home_img'] : '',0,'Hình ảnh','');
        $data_form[] = $form->textarea('news_home_des',isset($data['news_home_des']) ? $data['news_home_des'] : '',0,'Mô tả','');
         $data_form[] = $form->textarea('news_home_video',isset($data['news_home_video']) ? $data['news_home_video'] : '',0,'Đường dẫn video','');

          $data_form[] = $form->title('Cấu hình tin tức không video hiển thị trang chủ');
         $data_form[] = $form->image('news_home_img_1',isset($data['news_home_img_1']) ? $data['news_home_img_1'] : '',0,'Hình ảnh','');
         $data_form[] = $form->text('news_home_link_1',isset($data['news_home_link_1']) ? $data['news_home_link_1'] : '',0,'Url');
         $data_form[] = $form->textarea('news_home_des_1',isset($data['news_home_des_1']) ? $data['news_home_des_1'] : '',0,'Mô tả','');


        $data_form[] = $form->custom('admin.layouts.setting.home');

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title','data'));

    }
    public function general(Request $request) {
        $setting_name = 'general';
        $title = "Cấu hình chung";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        
        $data_form[] = $form->checkbox('robots',isset($data['robots']) ? $data['robots'] : '',1,' Cho phép google lập chỉ mục nội dung Website');//Cấu hình quan trọng
        $data_form[] = $form->textarea('html_head',isset($data['html_head']) ? $data['html_head'] : '',0,'Các thẻ html chèn vào head','');
        $data_form[] = $form->textarea('html_body',isset($data['html_body']) ? $data['html_body'] : '',0,'Các thẻ html chèn vào body','');

           $data_form[] = $form->image('image_favicon',isset($data['image_favicon']) ? $data['image_favicon'] : '',0,'ảnh favicon','');
       
        $data_form[] = $form->image('logo_header',isset($data['logo_header']) ? $data['logo_header'] : '',0,'Logo hiển thị header','');

        // $data_form[] = $form->image('logo_footer',isset($data['logo_footer']) ? $data['logo_footer'] : '',0,'Logo hiển thị chân trang','');
        
        $data_form[] = $form->title('Cấu hình mạng xã hội (chân trang)');
        $data_form[] = $form->text('link_facebook',isset($data['link_facebook']) ? $data['link_facebook'] : '',0,'Link facebook','');
        $data_form[] = $form->text('link_pinterest',isset($data['link_pinterest']) ? $data['link_pinterest'] : '',0,'Link pinterest','');
        $data_form[] = $form->text('link_youtube',isset($data['link_youtube']) ? $data['link_youtube'] : '',0,'Link youtube','');
        $data_form[] = $form->text('link_twiter',isset($data['link_twiter']) ? $data['link_twiter'] : '',0,'Link twiter','');


        $data_form[] = $form->title('Cấu hình email nhận thông báo');
        $data_form[] = $form->text('notification_email',isset($data['notification_email']) ? $data['notification_email'] : '',0,'Email nhận thông báo','');


        $data_form[] = $form->title('Cấu hình tin tức');
        $data_form[] = $form->text('title_news_1',isset($data['title_news_1']) ? $data['title_news_1'] : '',0,'Tiêu đề 1','');
          $data_form[] = $form->text('url_news_1',isset($data['url_news_1']) ? $data['url_news_1'] : '',0,'Url 1','');

         $data_form[] = $form->image('image_news_1',isset($data['image_news_1']) ? $data['image_news_1'] : '',0,'Hình ảnh  1','');
          $data_form[] = $form->textarea('des_news_1',isset($data['des_news_1']) ? $data['des_news_1'] : '',0,'Mô tả 1','');

           $data_form[] = $form->text('title_news_2',isset($data['title_news_2']) ? $data['title_news_2'] : '',0,'Tiêu đề 2','');
          $data_form[] = $form->text('url_news_2',isset($data['url_news_2']) ? $data['url_news_2'] : '',0,'Url 2','');

         $data_form[] = $form->image('image_news_2',isset($data['image_news_2']) ? $data['image_news_2'] : '',0,'Hình ảnh 2','');
          $data_form[] = $form->textarea('des_news_2',isset($data['des_news_2']) ? $data['des_news_2'] : '',0,'Mô tả 2','');


        $data_form[] = $form->title('Cấu hình thông tin');
        $data_form[] = $form->text('address',isset($data['address']) ? $data['address'] : '',0,'Địa chỉ','');
        $data_form[] = $form->text('hotline',isset($data['hotline']) ? $data['hotline'] : '',0,'Hotline','');
        $data_form[] = $form->text('fax',isset($data['fax']) ? $data['fax'] : '',0,'Fax','');
        $data_form[] = $form->text('email',isset($data['email']) ? $data['email'] : '',0,'Email','');


        $data_form[] = $form->customMenu('menu_primarys', isset($data['menu_primarys']) ? $data['menu_primarys'] : '','Cấu hình menu chính');
        $data_form[] = $form->customMenu('menu_footer', isset($data['menu_footer']) ? $data['menu_footer'] : '','Cấu hình menu footer');

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }

    public function google_shopping(Request $request) {
        $setting_name = 'google_shopping';
        $title = "Cấu hình google_shopping";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();

        $data_form[] = $form->custom('admin.layouts.google_shopping_info');
        
        $data_form[] = $form->text('brand',isset($data['brand']) ? $data['brand'] : '',0,'Thương hiệu mặc định','');
        $data_form[] = $form->text('category',isset($data['category']) ? $data['category'] : '',0,'Danh mục Google shopping mặc định','');
        $data_form[] = $form->select('instock',isset($data['instock']) ? $data['instock'] : '',0,'Tình trạng kho hàng mặc định',[''=>'Mặc định','còn hàng'=>'còn hàng','hết hàng'=>'hết hàng']);
        $data_form[] = $form->select('itemcondition',isset($data['itemcondition']) ? $data['itemcondition'] : '',0,'Tình trạng sản phẩm mặc định',[''=>'Mặc định','mới'=>'mới','cũ'=>'cũ']);

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }
    public function seo(Request $request) {
        $setting_name = 'seo';
        $title = "Cấu hình Nội dung Seo cho danh mục";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
       
        $detail_product = isset($data["detail_product"])?$data["detail_product"]:"";
        $title_product = isset($data["title_product"])?$data["title_product"]:"";
        $desc_product = isset($data["desc_product"])?$data["desc_product"]:"";
        $detail_news = isset($data["detail_news"])?$data["detail_news"]:"";
        $title_news = isset($data["title_news"])?$data["title_news"]:"";
        $desc_news = isset($data["desc_news"])?$data["desc_news"]:"";
        $list_seo = compact('detail_product','title_product','desc_product','detail_news','title_news','desc_news');
        $form = new MyForm();
        $data_form[] = $form->custom('admin.layouts.setting.seo');
        
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title','list_seo'));
    }
}
