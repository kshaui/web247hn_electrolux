<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Product;
use App\Filter;
use App\ProductFilter;
class ProductController extends Controller
{
   
    function __construct()
    {
        $this->module_name = 'sản phẩm';
        $this->table_name = 'products';
        $this->has_google_shopping = true;
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('product_categories');
        $array_categories = $categories->data_select_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Tên sản phẩm','string',1);
        $listdata->add('price','Giá','int',1);
        $listdata->add('category_id','Danh mục','array',1,$array_categories);
        $listdata->add('updated_at','Thời điểm cập nhật','range',1);
        $listdata->add('home','Ghim trang chủ','pins');
        $listdata->add('hot','Ghim hot','pins');
        $listdata->add('highlight','Ghim nổi bật','pins');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_categories'));
    }
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('product_categories');
        $array_categories = $categories->data_select_categories();

         // Bộ lọc
         $filters = Filter::where('status',1)->get();

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tên sản phẩm','',1,'slug');
        $data_form[] = $form->slug('slug','');
       
        $data_form[] = $form->select('category_id',0,1,'Danh mục cha',$array_categories);

        $data_form[] = $form->text('price','',0,'Giá mới');
        $data_form[] = $form->text('price_old','',0,'Giá cũ');

        $data_form[] = $form->text('sku','',0,'Mã sản phẩm');
        $data_form[] = $form->related('brands','',0,'Thương hiệu','Tìm theo tiêu đề thương hiệu ...','trademark');

       

        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->slide('slides','',0);
        $data_form[] = $form->related('related_products','',0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->related('related_news','',0,'Chọn 4 tin tức liên quan','Tìm theo tiêu đề tin ...','news');

        $data_form[] = $form->title('Hướng dẫn nhập liệu : mỗi khuyến mại sẽ bao trong 1 thẻ <p></p> , bên trong có <span></span> chứa tên khuyến mãi chữ đỏ , chữ đen bình thường nhập không bao trong thẻ nào , Đường dẫn sẽ bao trong <a href="đường link"> Tên đường link hiển thị</a> . 
        Ví dụ : <p><span class="gift">Tặng bình giữ nhiệt và lock trị giá 590.000đ</span> ( đến 30/12/2019 )<a href="javascrip:;"> Xem chi tiết</a></p>');  
  
        $data_form[] = $form->textarea('promotion','',0,'Khuyến mại');
       

        $data_form[] = $form->textarea('description','',0,'Thông tin cơ bản','Nhập các thông tin cơ bản, xuống dòng với mỗi thông tin mới');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        //specifications
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();
        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi
        $this->checkSlug($this->table_name, $data_form['slug']);

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');
        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng

        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if (isset($related_products)) {
            $related_products = implode(',',$related_products);
        }else {
            $related_products = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }

        //thương hiệu
        if (isset($brands)) {
            $brands = implode(',',$brands);
        }else {
            $brands = '';
        }

        $data_insert = compact('category_id','name','slug','price','price_old','brands','promotion','image','slides','description','detail','related_products','related_news','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);        

        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->googleShopping($id_insert,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $product_filters = ProductFilter::where('product_id',$id)->pluck('filter_id')->toArray();

        $filters = Filter::where('status',1)->get();
    
        $categories = new Categories('product_categories');
        $array_categories = $categories->data_select_categories();

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tên sản phẩm','',1,'slug');
        $data_form[] = $form->slug('slug',$data_edit->slug);
        $data_form[] = $form->select('category_id',$data_edit->category_id,1,'Danh mục',$array_categories);
        $data_form[] = $form->text('price',$data_edit->price,0,'Giá mới');
        $data_form[] = $form->text('price_old',$data_edit->price_old,0,'Giá cũ');

       
        $data_form[] = $form->text('sku',$data_edit->sku,0,'Mã sản phẩm','Mã sản phẩm');
        $data_form[] = $form->related('brands',$data_edit->brands,0,'Thương hiệu','Tìm theo tiêu đề thương hiệu ...','trademark');

        $data_form[] = $form->image('image',$data_edit->image,0);
        $data_form[] = $form->slide('slides',explode(',',$data_edit->slides),0);
        $data_form[] = $form->related('related_products',$data_edit->related_products,0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->related('related_news',$data_edit->related_news,0,'Chọn 4 tin tức liên quan','Tìm theo tiêu đề tin ...','news');

        $data_form[] = $form->textarea('promotion',$data_edit->promotion,0,'Nhập các khuyến mại');
      
        $data_form[] = $form->textarea('description',$data_edit->description,0,'Thông tin cơ bản','Nhập các thông tin cơ bản, xuống dòng với mỗi thông tin mới');
        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');

     

        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
       
        $data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$data_edit->slug));
        return view('admin.layouts.edit',compact('data_form','id','filters','product_filters'));
    }

    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');
        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);

        if (isset($slides) && $slides != '') {
            $slides = implode(',', $slides);
        }else {
            $slides = '';
        }
        if (isset($related_products)) {
            $related_products = implode(',',$related_products);
        }else {
            $related_products = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }
          //thương hiệu
        if (isset($brands)) {
            $brands = implode(',',$brands);
        }else {
            $brands = '';
        }

        $data_update = compact('category_id','name','slug','price','price_old','brands','promotion','image','slides','description','detail','related_products','related_news','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);


        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        $this->googleShopping($id,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);
        
        $old = [
            'category_id'=>$data_edit->category_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'price'=>$data_edit->price,
            'price_old'=>$data_edit->price_old,
            'image'=>$data_edit->image,
            'slides'=>$data_edit->slides,
          
            'description'=>$data_edit->description,
            'detail'=>$data_edit->detail,
            'related_products'=>$data_edit->related_products,
            'related_news'=>$data_edit->related_news,
            'sku'=>$data_edit->sku,
            'brands'=>$data_edit->brands,
            'promotion'=>$data_edit->promotion,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
