<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Service;
use App\News;
use DB;
class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Dịch vụ',
            'description'=> 'Dịch vụ',
        ]);
        $breadcrumbs = [
            ['name'=> 'dịch vụ','url' => route('web.services.index')],               
        ];
        
        // lấy ra các tin được ghim cho trang dịch vụ
        // $news = News::where('status',1)
        // ->leftJoin('pins', 'news.id','=','pins.type_id')
        // ->where('pins.type','news')
        // ->where('pins.place','service')
        // ->orderBy('value', 'asc')
        // ->limit(3)
        // ->get();

        // if (count($news) < 0) {
            $news = News::where('status',1)->orderBy('created_at', 'desc')->limit(3)->get();
        // }

        return view('web.services.index', compact('breadcrumbs','services','meta_seo','news'));
    }
    public function show($slug)
    {
        $service = Service::where('status',1)->where('slug',$slug)->firstOrFail();

        $service_question_maps ='';
        if(!empty($service)){
        $service_question_maps = DB::table('service_question_maps')->where('service_id', $service->id)->get();
        }

        $news = News::where('status',1)->orderBy('created_at','desc')->limit(3)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => $service->getName(),
            'description'=> $service->getDes(120),
        ]);
        $breadcrumbs = [
             ['name'=> 'dịch vụ','url' => route('web.services.index')], 
            ['name'=> $service->getName(),'url' => '#'],               
        ];
       
        return view('web.services.show', compact('breadcrumbs','service','meta_seo','news','service_question_maps'));
    }
}
