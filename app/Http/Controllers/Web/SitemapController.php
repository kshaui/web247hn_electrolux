<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\Category;
use App\Classe;
use App\NewsCategory;
use App\Service;
use App\News;

class SitemapController extends Controller
{
    public $domain;
    public $sitemap = '';
    public $page_size = 200;
    public $lastmod;

    public function __construct()
    {
        Debugbar::disable();
        $this->domain = config('app.url');
        $this->lastmod = date('Y-m-d');
    }

    public function build($template) {
        $content = '<?xml version="1.0" encoding="UTF-8"?>';
        $content .= '<?xml-stylesheet type="text/xsl" href="/public/assets/web247hn-sitemap.xsl"?>';
        $content .= '<'.$template.' xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
        '.$this->sitemap.'
        </'.$template.'>';
        return response($content, '200')->header('Content-Type', 'text/xml');
    }

    public function index() {
        $this->sitemap .= '<sitemap>
    		<loc>'.$this->domain.'/sitemap-misc.xml</loc>
    		<lastmod>'.$this->lastmod.'</lastmod>
    	</sitemap>';

        $this->sitemap .= '<sitemap>
    		<loc>'.$this->domain.'/sitemap-service-categories.xml</loc>
    		<lastmod>'.$this->lastmod.'</lastmod>
    	</sitemap>';

        $this->sitemap .= '<sitemap>
    		<loc>'.$this->domain.'/sitemap-class-categories.xml</loc>
    		<lastmod>'.$this->lastmod.'</lastmod>
    	</sitemap>';

        $this->sitemap .= '<sitemap>
    		<loc>'.$this->domain.'/sitemap-news-categories.xml</loc>
    		<lastmod>'.$this->lastmod.'</lastmod>
    	</sitemap>';

        //Các dịch vụ
        $total_service = Service::where('status',1)->where('instock','<>',3)->count();
        if($total_service%($this->page_size) == 0) {
            $page_total = $total_service/($this->page_size);
        }else {
            $page_total = (int)($total_service/($this->page_size)) + 1;
        }
        for($i=1;$i<=$page_total;$i++) {
            $this->sitemap .= '<sitemap>
                    <loc>'.$this->domain.'/sitemap-service-page-'.$i.'.xml</loc>
                    <lastmod>'.$this->lastmod.'</lastmod>
                </sitemap>';
        }

        //Các tin
        $total_news = News::where('status',1)->count();
        if($total_news%($this->page_size) == 0) {
            $page_total = $total_news/($this->page_size);
        }else {
            $page_total = (int)($total_news/($this->page_size)) + 1;
        }
        for($i=1;$i<=$page_total;$i++) {
            $this->sitemap .= '<sitemap>
                    <loc>'.$this->domain.'/sitemap-news-page-'.$i.'.xml</loc>
                    <lastmod>'.$this->lastmod.'</lastmod>
                </sitemap>';
        }

        return $this->build('sitemapindex');
    }

    public function misc() {
        $this->sitemap .= '<url>
                            <loc>'.$this->domain.'</loc>
                            <lastmod>'.$this->lastmod.'</lastmod>
                            <changefreq>daily</changefreq>
                            <priority>1.00</priority>
                          </url>';
        $this->sitemap .= '<url>
                            <loc>'.$this->domain.'/sitemap.xml</loc>
                            <lastmod>'.$this->lastmod.'</lastmod>
                            <changefreq>monthly</changefreq>
                            <priority>0.5</priority>
                          </url>';

        return $this->build('urlset');
    }

   

    public function newsCategories() {
        $categories = NewsCategory::where('status',1)->orderBy('order','ASC')->get();
        foreach ($categories as $value) {
            $this->sitemap .= '<url>';
            $this->sitemap .= '<loc>'.$this->domain.url_category($value->slug).'</loc>';
            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';
            $this->sitemap .= '<changefreq>weekly</changefreq>';
            $this->sitemap .= '<priority>0.80</priority>';
            $this->sitemap .= '</url>';
        }
        return $this->build('urlset');
    }

    public function show($slug,$page) {
        $page_start = ($page - 1)*$this->page_size;
        switch ($slug) {
            case 'service':
                $services = Service::where('status',1)->where('instock','<>',3)->orderBy('created_at', 'desc')->limit($this->page_size)->offset($page_start)->get();
                foreach ($services as $value) {
                    $this->sitemap .= '<url>';
                    $this->sitemap .= '<loc>'.$this->domain.$value->getRewrite().'</loc>';
                    $this->sitemap .= '<lastmod>'.date('Y-m-d',strtotime($value->updated_at)).'</lastmod>';
                    $this->sitemap .= '<changefreq>weekly</changefreq>';
                    $this->sitemap .= '<priority>0.80</priority>';
                    $this->sitemap .= '</url>';
                }
                break;
            case 'news':
                $news = News::where('status',1)->orderBy('created_at', 'desc')->limit($this->page_size)->offset($page_start)->get();
                foreach ($news as $value) {
                    $this->sitemap .= '<url>';
                    $this->sitemap .= '<loc>'.$this->domain.$value->getRewrite().'</loc>';
                    $this->sitemap .= '<lastmod>'.date('Y-m-d',strtotime($value->updated_at)).'</lastmod>';
                    $this->sitemap .= '<changefreq>weekly</changefreq>';
                    $this->sitemap .= '<priority>0.80</priority>';
                    $this->sitemap .= '</url>';
                }
                break;
        }
        return $this->build('urlset');
    }
}
