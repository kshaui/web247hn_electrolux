<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Product;
use App\ProductCategory;
use App\Comment;
class ProductController extends Controller
{
    public function AllShow(Request $request)
    {
        $filter_price = $request->price_filter;
        $data = Product::where('status',1);

        if($filter_price != ''){
            switch ($filter_price){
                case '1':
                    $data = $data->where('price','<',3000000);
                    break;
                case '2':
                    $data = $data->whereBetween('price',[3000000,5000000]);
                    break;
                case '3':
                    $data = $data->whereBetween('price',[5000000,10000000]);
                    break;
                case '4':
                    $data = $data->whereBetween('price',[10000000,15000000]);
                    break;
                case '5':
                    $data = $data->whereBetween('price',[15000000,20000000]);
                    break;
                case '6':
                    $data = $data->where('price','>',20000000);
                    break;
            }
        }

        $data = $data->paginate(20);
        $product_categories = ProductCategory::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,
        [
            'title' => 'Sản phẩm',
            'description'=> 'Sản phẩm',
            'url' => url('').'/san-pham',
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],         
        ];
        return view('web.products.index',compact('data','meta_seo','breadcrumbs','product_categories','filter_price'));
    }
    public function index(Request $request,$slug)
    { 
        $filter_price = $request->price_filter;
        
        $category = ProductCategory::where('status',1)->where('slug',$slug)->firstOrFail();
        $product_categories = ProductCategory::where('status',1)->get();
        $data = Product::where('status',1)->where('category_id',$category->id);

        if($filter_price != ''){
            switch ($filter_price){
                case '1':
                    $data = $data->where('price','<',3000000);
                    break;
                case '2':
                    $data = $data->whereBetween('price',[3000000,5000000]);
                    break;
                case '3':
                    $data = $data->whereBetween('price',[5000000,10000000]);
                    break;
                case '4':
                    $data = $data->whereBetween('price',[10000000,15000000]);
                    break;
                case '5':
                    $data = $data->whereBetween('price',[15000000,20000000]);
                    break;
                case '6':
                    $data = $data->where('price','>',20000000);
                    break;
            }
        }

        $data = $data->paginate(20);

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $category->getName(),
            'description'=> $category->getDes(160),
            'url' => $category->getUrl(),
            'image'=> $category->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $category->getName(),'url' =>  $category->getUrl()],         
        ];


        return view('web.products.index',compact('data','meta_seo','breadcrumbs','product_categories','slug','filter_price'));
    }
    public function show($slug)
    {
        $product = Product::where('status',1)->where('slug',$slug)->firstOrFail();

        if (isset($product->slides) && $product->slides != '') {
            $slides = explode(",",$product->slides);
        }else {
            $slides = '';
        }
        if(isset($product->promotion) && $product->promotion != ''){
            $gifts = explode(PHP_EOL, $product->promotion??'');
        }else{
            $gifts = '';
        }
       

        $product_category = ProductCategory::where('status',1)->where('id',$product->category_id)->firstOrFail();


        if ($product->related_products != '') {
            //sản phẩm liên quan
            $related_product = $product->get_related_product();
        }

        // Nếu không có sp lq được chọn lấy auto 10 sp
        if ($product->related_products == '') {
            $related_product = Product::where('status',1)->where('category_id',$product->category_id)->limit(4)->get();
        }

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $product->getName(),
            'description'=> $product->getDes(160),
            'url' => $product->getUrl(),
            'image'=> $product->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $product_category->getName(),'url' =>  $product_category->getUrl()],         
        ];
        $admin_bar_edit = route('products.edit', $product->id);

        //Bình luận cha
        $comment_values = Comment::where('type_id',$product->id)->where('type','products')->where('parent_id',0)->where('status',1)->orderBy('created_at','DESC')->paginate(5);
        //Trả lời bình luận
        $comment_reply = Comment::whereIn('parent_id',$comment_values->pluck('id'))->where('status',1)
        ->get();

        return view('web.products.show',compact('product','meta_seo','breadcrumbs','product_category','slides','related_product','admin_bar_edit','gifts','comment_values','comment_reply'));
    }
}
