<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\MyClass\Categories;
use Cache;
use DB;
use Cart;
use App\Order;
use App\OrderDetail;
use Mail;
use Session;
use App\Contact;
use App\Comment;

use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    //bình luận sản phẩm
    public function createComment(Request $request)
    {
        $data = $request->all();
       
        $create = Comment::create([
            'parent_id'=>0,
            'type'=>$data['type'],
            'type_id'=>$data['type_id'],
            'url'=>0,
            'user_id'=>0,
            'admin_id'=>0,
            'gender'=>$data['gender'],
            'name'=>$data['name'],
            'email'=>$data['email'],
            'phone'=>$data['phone'],
            'like'=>0,
            'rank'=>5,
            'content'=>$data['content'],
            'status'=>2,
        ]);
    }
    public function send_contact(Request $request){
        $validate = Validator::make(
            $request->all(),
            [
             'email' => 'required',
             'phone' => 'required',
             'name' => 'required',
            ],
            [
             'email.required' => 'Email không được để trống',
             'phone.required' => 'Số điện thoại không được để trống',
             'note.required' => 'Tên không được để trống',
            ]
        );

        
        if ($validate->fails()) {
            return response(
                [
                    'errors' => true,
                    'data' => $validate->errors(),
                ]
            );
        } else {
            $input = $request->all();
            $create = Contact::create([
                'email'=>$input['email'],
                'phone'=>$input['phone'],
                'name'=>$input['name'],
                'status'=>2,
            ]);

            // Mail::send('mailfb', array('phone'=>$input["phone"],'email'=>$input["email"], 'name'=>$input['name']), function($message){
            //     $message->to('tuananhnguyen199dz@gmail.com', 'Yêu cầu liên hệ từ elextrolux')->subject('Khách hàng yêu cầu liên hệ hỗ trợ sản phẩm!');
            // });
            Session::flash('flash_message', 'Send message successfully!');
            return response(
                [
                    'errors' => false,
                    'data' => '1',
                ]
            );
        }
    }
    // lưu đơn hàng
    public function cartOrder(Request $request)
    {
        $data = $request->all();

        if($request->discount_code != ''){
            $discount_code = $request->discount_code;
        }else{
            $discount_code = 0;
        }

        $order_id = Order::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'price_total'=>Cart::total(),
            'adress'=>$data['adress'],
            'discount_code'=>$discount_code,
            'phone'=>$data['phone'],
            'payment_method'=>1,
            'note'=>$data['notes'],
            'status'=>1,
        ])->id;


        $timestamps = false;
        foreach(Cart::content() as $ca)
        $create = OrderDetail::create([
            'quanity'=>$ca->qty,
            'price'=>$ca->price,
            'price_total'=>$ca->price * $ca->qty,
            'order_id'=>$order_id,
            'discount_code'=>$discount_code,
            'product_id'=>$ca->id,
            'type'=>2,
        ]);
        Cart::destroy();
        return response(
            [
                'errors' => false,
                'data' => '1',
            ]
        );

    }

    // load quận huyện ajax
    public function getDistrict(Request $request){

        if($request->ajax()){
            $province_id=$request->province_id;
            $province = DB::table('province')->where('id',$province_id)->first();
            $district = DB::table('district')->where('parent_id', $province_id)->get();
            $result =  [];
            $result['district'] = '<option value="">Chọn quận/huyện</option>';
            
            foreach($district as $key => $value){
                $result['district'] .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            $result['status'] = 1;
            return json_encode($result);
        }
    }

    // xóa sp khỏi giỏ
    public function removeCart(Request $request){
        if ($request->ajax()) {
            $rowId = $request->rowId;

            Cart::remove($rowId);
            $result['count_update'] = Cart::count();
            $result['count_cart'] = Cart::count();
            $result['price'] = Cart::total().' VNĐ';
            return response()->json($result);
        }   
    }

    //thêm vào giỏ hàng
    public function Order(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $data = DB::table($type)->where('id',$id)->first();

            $name = $data->name;
            $price = $data->price??0;
            $image = $data->image;
            $cart_info = [
                "id" => $id,
                "name" => $name,
                "price" => $price,
                "weight"=>25,
                "qty" => $qty,
                "options" => [
                    'type' => $type,
                    'image' => $image,
                    'slug' => $data->slug,
                ],
                
            ];
            
            Cart::add($cart_info);

            $cart_content = Cart::content();
            $cart_count = Cart::count();
            $cart_total = Cart::total();

            return response(json_encode(compact('cart_content','cart_count','cart_total','qty','image','name','price')));
        }
    }

     /**
     * đăng nhập
     */
    public function AjaxLogin(Request $request)
    {
        if ($request->ajax()) {
            $email = $request->email_login;
            $password = $request->password_login;
            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
                return 1;
            } else {
                if (Auth::attempt(['name' => $email, 'password' => $password, 'status' => 1])) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
    /**
     * đăng ký
     */
    public function AjaxRegister(Request $request)
    {
      
        if ($request->ajax()) {
            $name = $request->name_registration;
            $surname = $request->surname_registration;
            $email = $request->email_registration;
            $password = $request->password_registration;
            $check_email = DB::table('users')->where('email', $email)->where('status', '!=', 4)->first();
            $check_name = DB::table('users')->where('name', $name)->where('status', '!=', 4)->first();
            $today = date("Y-m-d H:i:s");
            $data['email'] = $email;
            $data['name'] = $name;

            $result['status'] = 0;
            $result['message'] = "";
            if (!empty($check_name)) {
                $result['message'] = "Tên đăng nhập đã được sử dụng";
                $result['status'] = -1;
            } elseif (!empty($check_email)) {
                $result['message'] = "Email đã được sử dụng";
                $result['status'] = 0;
            } else {
                $check = DB::table('users')->where('email', $email)->where('status', 4)->first();
                if (!empty($check)) {
                    DB::table('users')->where('id', $check->id)->delete();
                }
                $db_insert = [
                    'name' => $name,
                    'surname'=>$surname,
                    'email' => $email,
                    'password' => bcrypt($password),
                    'created_at' => $today,
                    'updated_at' => $today,
                    'image' => '',
                    'fullname' => '',
                    'phone' => '',
                    'status' => 2,
                ];
                $id_insert = DB::table('users')->insertGetId($db_insert);
                // $data['id'] = $id_insert;
                // $data['password'] = $password;

                // Mail::to($email)->send(new comfirmEmail($data));
                $result['status'] = 1;
            }
            return json_encode($result);
        }
    }
}
