<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Product;
use App\News;
class SearchController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->all());
        // if (isset($_GET['keyword'])) {
        //     $keyword = $_GET['keyword'];
        //     $key = str_replace(" ","%",$keyword);
        //     $key = str_replace("\'","'",$key);
        //     $key = str_replace("'","''",$key);
        // } else {
        //     $keyword = $key = "";
        // }


        $search = $request->search;
        $search        = str_replace("\'","'",$search);
        $search        = str_replace("'","''",$search);

        $breadcrumbs = [
            ['name'=> 'Tìm kiếm','url' => '/tim-kiem'],         
        ];

        // $services = Service::join('service_categories','service_categories.id','services.category_id')
        //             ->where( 'services.name', 'LIKE', "%$key%")
        //             ->where('services.status',1)
        //             // ->where('service_categories.status',1)
        //             ->select('services.*')
        //             ->take(20)->get();

        // $fits = Fit::join('fit_categories','fit_categories.id','fits.category_id')
        //             ->where('fits.name', 'LIKE', "%$key%")
        //             ->where('fits.status',1)
        //             // ->where('fit_categories.status',1)
        //             ->select('fits.*')
        //             ->take(20)->get();

        // dd($key);

        $products = Product::where('status',1)->where('name','like','%'.$search.'%')->limit(16)->get();

        $news = News::join('news_categories','news_categories.id','news.category_id')
                    ->where('news.name', 'LIKE', "%$search%")
                    ->where('news.category_id','!=',0)
                    ->where('news.status',1)
                    // ->where('news_categories.status',1)
                    ->select('news.*')
                    ->take(10)->get();  
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Tìm kiếm'.' - Cityphone',
            'description'=> 'Tìm kiếm',
        ]);
        return view('web.search.index', compact('keyword','news','meta_seo','products','breadcrumbs','search'));
    }
}
