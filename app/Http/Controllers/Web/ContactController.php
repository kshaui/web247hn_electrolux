<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
class ContactController extends Controller
{
    public function index()
    {
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Liên hệ',
            'description'=> '',
        ]);
        $breadcrumbs = [
            ['name'=> 'Liên hệ','url' => '/lien-he'],               
        ];
        return view('web.contacts.index', compact('meta_seo','breadcrumbs'));
    }
}
