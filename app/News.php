<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function getUrl() {
    	return route('web.news.show',$this->slug);
    }
    public function getUrlMb() {
        return route('mobile.news.show',$this->slug);
    }
    public function getImage($size = '') {
    	// $size: tiny 80, small 150, medium 300, large 600
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }
    public function getName()
    {
        return cutString(removeHTML($this->name),80);
    }
    public function getName_Mb()
    {
        return cutString(removeHTML($this->name),50);
    }
    public function getDes($size)
    {
        return cutString(removeHTML($this->detail),$size);
    }
}
