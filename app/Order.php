<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable  = ['name','email','adress','phone','provincial','district','note','payment_method','total_price','status'];

    public static function add($request)
    {
        $created_at = $updated_at = date('Y-m-d H:i:s');
    	$order = new order;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->adress = $request->adress;
        $order->phone = $request->phone;
        $order->provincial = $request->provincial;
        $order->district = $request->district;
        $order->note = $request->note;
        $order->payment_method = 1;
        $order->status = 1;
        $order->created_at = $created_at;
        $order->updated_at = $updated_at;
        $order->save();
    }
}
