<?php

namespace App\MyClass;
//require rackspace/php-opencloud
use OpenCloud\OpenStack;

class ObjectStorageService {
    private $client;
    private $service_name;
    private $region;
    private $container;
    private $container_name;
    private $container_url;

    public function __construct($config){
        //http://docs.php-opencloud.com/en/latest/services/object-store/index.html#setup
        $this->client = new OpenStack($config['endpoint'], array(
            'username' => $config['username'],
            'password' => $config['password'],
            'tenantId' => $config['tenant_id'],
        ));
        $this->service_name = $config['service_name'];
        $this->region = $config['region'];
        $this->container_name = $config['container'];
        $this->container_url = $config['container_url'];
    }

    private function getContainer(){
        if (!$this->container){
            $this->container = $this->client->objectStoreService($this->service_name, $this->region, 'publicURL')->getContainer($this->container_name);
        }
        return $this->container;
    }

    public function createContainer($name) {
        $container = $this->client->objectStoreService($this->service_name, $this->region, 'publicURL')->createContainer($name);
        return $container;
    }

    public function fileGet($filename){
        $url = $this->getContainer()->getObject($filename)->getUrl();
        return $url;
    }

    public function fileGetUrl($filename){
        $url = $this->container_url.'/'.$filename;
        return $url;
    }

    public function filePut($file, $filename = null){
        $getPath = null;
        $isString = is_string($file);
        if($isString){
            $getPath = $file;
        }else{
            $getPath = $file->getRealPath();
        }
        if($filename == null){
            if($isString){
                $explodePath = explode("/", $file);
                $filename = $explodePath[count($explodePath)-1];
            }else{
                $filename = $file->getClientOriginalName();
            }
        }
        $this->getContainer()->uploadObject($filename, fopen($getPath, 'r'));
        //$url = Storage::disk('sop')->filePut('C:\\Users\TuanTai\\Downloads\\anh.jpg','2018/02/test.jpg');
    }

    public function filePut2($file, $filename = null){
        $upload = $this->getContainer()->uploadObject($filename, $file);
        return $upload;
    }

    public function fileExists($filename){
        foreach($this->getContainer()->objectList() as $obj){
            if($obj->getName() == $filename){
                return true;
            }
        }
        return false;
    }

    public function fileList(){
        return $this->getContainer()->objectList();
    }

    public function fileDelete($filename){
        $object = $this->getContainer()->getObject($filename);
        return $object->delete();
    }
}