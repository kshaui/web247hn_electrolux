<?php
namespace App\Providers;

use Storage;
use Illuminate\Support\ServiceProvider;
use App\MyClass\ObjectStorageService;

class SudoOpsServiceProvider extends ServiceProvider {
    public function boot() {
        Storage::extend('sop',function ($app, $config) {
            $client = new ObjectStorageService($config);
            return $client;
        });
    }
    public function register()
    {
        //
    }
}