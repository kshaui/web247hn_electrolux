<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    protected $fillable  = ['quanity','price','price_total','order_id','product_id','type'];
    public $timestamps = false;
    public static function add($request)
    {
    	$orderDetail = new order;
        $orderDetail->quanity = $request->quanity;
        $orderDetail->price = $request->price;
        $orderDetail->price_total = $request->price_total;
        $orderDetail->order_id = $request->order_id;
        $orderDetail->product_id = $request->product_id;
        $orderDetail->type = $request->type;
        $timestamps = false;
        $order->save();
    }
}
