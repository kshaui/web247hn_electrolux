<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'district';
    protected $fillable  = ['parent_id','name','slug','title_seo','detail'];
}
