<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable  = ['email','phone','name','status'];

    public static function add($request)
    {
        $created_at = $updated_at = date('Y-m-d H:i:s');
    	$contact = new Contact;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->name = $request->name;
        $contact->created_at = $created_at;
        $contact->updated_at = $updated_at;
        $contact->save();
    }
}
