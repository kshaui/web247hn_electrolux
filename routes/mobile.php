<?php

/*
|--------------------------------------------------------------------------
| Mobile Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Mobile routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Mobile" middleware group. Now create something great!
|
*/

/**
 * ajax
 */

Route::get('/amp', 'Mobile\HomeController@indexAmp')->name('mobile.home.amp');
Route::get('/amp/{slug}.html','Mobile\ServiceController@showAmp')->name('mobile.service.show.amp');
Route::get('/amp/{category_slug}/{slug}.html','Mobile\NewsController@showAmp')->name('mobile.news.show.amp');
Route::get('/amp/{slug1?}/{slug2?}/{slug3?}/{slug4?}/{slug5?}','Mobile\Controller@categoryAmp')->name('mobile.category.amp');




Route::group(['prefix' => 'ajax'], function() {
	Route::post('register','Mobile\AjaxController@AjaxRegister')->name('mobile.ajax.register');
	Route::post('login','Mobile\AjaxController@AjaxLogin')->name('mobile.ajax.login');

	/**
	 * dat hang
	 */

	// gửi yêu cầu liên hệ
	Route::post('send-contact', 'Mobile\AjaxController@send_contact')->name('mobile.contact.send');
	
	Route::post('dat-hang','Mobile\AjaxController@Order')->name('mobile.ajax.order');
	// cập nhật số lượng sản phẩm qua button + - 
	Route::post('/edit-cart','Mobile\OrderController@updateQty')->name('cart.update.qty');
	// Xóa sản phẩm khỏi giỏ hàng
	Route::post('/xoa-san-pham', 'Mobile\AjaxController@removeCart')->name('ajax.remove.cart');
	// Đặt hàng
	Route::post('/luu-don-hang','Mobile\AjaxController@cartOrder')->name('cart.order');
	// chọn tỉnh quận huyện
	Route::post('/dia-chi','Mobile\AjaxController@getDistrict')->name('cart.district');
});


route::get('error-404.html', 'Mobile\HomeController@error404')->name('mobile.error404');


Route::get('/ho-tro','Mobile\SupportController@index')->name('support.index');
// chi tiết hỗ trợ
Route::get('/ho-tro/{slug}.html','Mobile\SupportController@show')->name('support.index.show');

// show giỏ hàng 
Route::get('/gio-hang','Mobile\OrderController@showCart')->name('mobile.cart.show');

// cập nhật số lượng sản phẩm qua button + - 
Route::get('/cap-nhat-so-luong-sp','Mobile\OrderController@updateQty')->name('cart.update.qty');

// điều hướng từ trang show giỏ hàng về đặt hàng  
Route::get('/dat-hang','Mobile\OrderController@cartAdd')->name('cart.add');

// điều hướng về trang đặt hàng thành công
Route::get('/dat-hang-thanh-cong','Mobile\OrderController@orderSucces')->name('cart.success');

// like cmt
Route::get('/like','Mobile\AjaxComments@like')->name('comment.like');
// ajax comments
Route::post('/binh-luan','Mobile\AjaxController@createComment')->name('mobile.ajax.comments');



Route::get('datafeeds','Mobile\HomeController@datafeeds');

Route::get('/', 'Mobile\HomeController@index')->name('mobile.home');
Route::get('/test', 'Mobile\TestController@index')->name('mobile.test');

Route::get('tim-kiem','Mobile\SearchController@index')->name('mobile.search.index');

Route::get('page-{slug}.html','Mobile\PageController@show')->name('mobile.pages.show');

Route::get('lien-he','Mobile\ContactController@index')->name('mobile.contact.index');


Route::get('dich-vu-{slug}.html','Mobile\ServiceController@show')->name('mobile.services.show');
Route::get('dich-vu','Mobile\ServiceController@index')->name('mobile.services.index');

Route::get('tin-tuc-{slug}.html','Mobile\NewsController@show')->name('mobile.news.show');
Route::get('tin-tuc-{slug}','Mobile\NewsController@index')->name('mobile.news_categories.show');
Route::get('tin-tuc','Mobile\NewsController@AllShow')->name('mobile.news_categories.Allshow');

Route::get('{slug}.html','Mobile\ProductController@show')->name('mobile.products.show');
Route::get('san-pham','Mobile\ProductController@AllShow')->name('mobile.product_categories.allshow');
Route::get('san-pham-{slug}','Mobile\ProductController@index')->name('mobile.product_categories.show');
