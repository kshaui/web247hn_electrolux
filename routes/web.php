<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * ajax
 */

Route::get('/amp', 'Mobile\HomeController@indexAmp')->name('mobile.home.amp');
Route::get('/amp/{slug}.html','Mobile\ServiceController@showAmp')->name('mobile.service.show.amp');
Route::get('/amp/{category_slug}/{slug}.html','Mobile\NewsController@showAmp')->name('mobile.news.show.amp');
Route::get('/amp/{slug1?}/{slug2?}/{slug3?}/{slug4?}/{slug5?}','Mobile\Controller@categoryAmp')->name('mobile.category.amp');

Route::get('sitemap.xml','Web\SitemapController@index');
Route::get('sitemap-misc.xml','Web\SitemapController@misc');
Route::get('sitemap-service-categories.xml','Web\SitemapController@serviceCategories');
Route::get('sitemap-class-categories.xml','Web\SitemapController@classCategories');
Route::get('sitemap-news-categories.xml','Web\SitemapController@newsCategories');
Route::get('sitemap-{slug}-page-{page}.xml','Web\SitemapController@show')->where(['slug' => '([^/]*)', 'page' => '[0-9]+']);


Route::group(['prefix' => 'ajax'], function() {
	Route::post('register','Web\AjaxController@AjaxRegister')->name('web.ajax.register');
	Route::post('login','Web\AjaxController@AjaxLogin')->name('web.ajax.login');

	/**
	 * dat hang
	 */
	// gửi yêu cầu liên hệ
	Route::post('send-contacts', 'Web\AjaxController@send_contact')->name('web.contact.send');

	Route::post('dat-hang','Web\AjaxController@Order')->name('web.ajax.order');
	// cập nhật số lượng sản phẩm qua button + - 
	Route::post('/edit-cart','Web\OrderController@updateQty')->name('cart.update.qty');
	// Xóa sản phẩm khỏi giỏ hàng
	Route::post('/xoa-san-pham', 'Web\AjaxController@removeCart')->name('ajax.remove.cart');
	// Đặt hàng
	Route::post('/luu-don-hang','Web\AjaxController@cartOrder')->name('cart.order');
	// chọn tỉnh quận huyện
	Route::post('/dia-chi','Web\AjaxController@getDistrict')->name('cart.district');
});


route::get('error-404.html', 'Web\HomeController@error404')->name('web.error404');


Route::get('/ho-tro','Web\SupportController@index')->name('support.index');
// chi tiết hỗ trợ
Route::get('/ho-tro/{slug}.html','Web\SupportController@show')->name('support.index.show');

// show giỏ hàng 
Route::get('/gio-hang','Web\OrderController@showCart')->name('cart.show');

// cập nhật số lượng sản phẩm qua button + - 
Route::get('/cap-nhat-so-luong-sp','Web\OrderController@updateQty')->name('cart.update.qty');

// điều hướng từ trang show giỏ hàng về đặt hàng  
Route::get('/dat-hang','Web\OrderController@cartAdd')->name('cart.add');

// điều hướng về trang đặt hàng thành công
Route::get('/dat-hang-thanh-cong','Web\OrderController@orderSucces')->name('cart.success');

// like cmt
Route::get('/like','Web\AjaxComments@like')->name('comment.like');

// ajax comments
Route::post('/binh-luan','Web\AjaxController@createComment')->name('web.ajax.comments');


Route::get('datafeeds','Web\HomeController@datafeeds');

Route::get('/', 'Web\HomeController@index')->name('web.home');
Route::get('/test', 'Web\TestController@index')->name('web.test');

Route::get('tim-kiem','Web\SearchController@index')->name('web.search.index');

Route::get('page-{slug}.html','Web\PageController@show')->name('web.pages.show');

Route::get('lien-he','Web\ContactController@index')->name('web.contact.index');


Route::get('dich-vu-{slug}.html','Web\ServiceController@show')->name('web.services.show');
Route::get('dich-vu','Web\ServiceController@index')->name('web.services.index');

Route::get('tin-tuc-{slug}.html','Web\NewsController@show')->name('web.news.show');
Route::get('tin-tuc-{slug}','Web\NewsController@index')->name('web.news_categories.show');
Route::get('tin-tuc','Web\NewsController@AllShow')->name('web.news_categories.Allshow');

Route::get('{slug}.html','Web\ProductController@show')->name('web.products.show');
Route::get('san-pham','Web\ProductController@AllShow')->name('web.product_categories.allshow');
Route::get('san-pham-{slug}','Web\ProductController@index')->name('web.product_categories.show');
