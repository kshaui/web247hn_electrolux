@extends('mobile.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "web247hn thiết kế web chuyên nghiệp",
        "logo": "{{config('app.url')}}/public/assets/img/logo.png"
    }
</script>
@endsection

@section('content')
<div class="inspiration">
    <div class="container">
        <div class="service-inspired">
            <div class="service-inspired__list">
                @include('mobile.news.item-news',['data'=>$data])
            </div>
            <div class="pagePaginate">
                {{ $data->links()??'' }}
            </div>
        </div>
    </div>
</div>
@endsection