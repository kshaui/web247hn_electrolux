@if(isset($data) && count($data) )
@foreach($data as $value)
<div class="item">
    <div class="item-img">
        <a href="{{ $value->getUrlMb() }}">
            <img src="{{ $value->getImage('large') }}" {{ $value->slug }}>
        </a>
    </div>
    <div class="item-infor">
        <div class="item-infor__title">
            <h3 id="title">
                <a href="{{ $value->getUrlMb() }}">
                    {{ $value->getName_Mb() }}
                </a>
            </h3>
        </div>
        <div class="item-infor__desc">
            <p>{{$value->getDes('50')}}</p>
        </div>
    </div>
</div>
@endforeach
@endif