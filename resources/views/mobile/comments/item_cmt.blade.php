<div class="newsDeatail-left__cmt">
    <div class="item-title">
        <p>BÌNH LUẬN</p>
    </div>
    <form action="{{ route('mobile.ajax.comments',$post_data->id) }}" id="create_comment_new" name="myform" method="post" role="form">
        @csrf
        <textarea name="content" placeholder="Nhập bình luận của bạn ..."></textarea>
        <div class="item-send">
            <a href="javascript:;" class="btn-sendcmt">GỬI</a>
        </div>
        <div class="form">
            <input type="hidden" value="{{ $name }}" name="type">
            <input type="hidden" value="{{ $post_data->id }}" name="type_id">
            <input type="hidden" id="gender" value="1" name="gender">
            <input type="radio" class="form-check" value="1" name="gender"> <label class="form-title">Anh</label>
            <input type="radio" class="form-check" value="2" name="gender"> <label class="form-title">Chị</label> 
            <input type="radio" class="form-check" value="3" name="gender"> <label class="form-title">Khác</label> <br><br>
            <input type="text" placeholder="Họ và tên" name="name">
            <input type="text" placeholder="Số điện thoại" name="phone">
            <input type="text" placeholder="Email" name="email">
            <button id="putComment" name="btn" type="button" class="putComment">GỬI BÌNH LUẬN</button>
        </div>
    </form>
</div>
<div class="newsDeatail-left__showCmt">
    @foreach($comment_values as $val)
    <div class="listCmt">
        <div class="listCmt-avt">
            <span>{!! mb_substr($val->name,0,1) !!}</span>
        </div>
        <div class="listCmt-info">
            <div class="item-name">
                <strong>{{$val->name ??''}} <label>{!! mb_substr($val->phone,0,7) !!}xxx</label></strong>
            </div>
            <div class="item-content">
                <p>{{$val->content ??''}}</p>
            </div>
            <div class="item-time">
                <p>{!! change_time_to_text($val->created_at) !!}</p>
            </div>
        </div>
    </div>
    @if(count($comment_reply->where('parent_id',$val->id))  > 0)
        @foreach($comment_reply as $value)
        @if($value->parent_id == $val->id)
        <div class="listReply">
            <div class="listCmt-avt">
                <span>{!! mb_substr($value->name,0,1) !!}</span>
            </div>
            <div class="listCmt-info">
                <div class="item-name">
                    <strong>{{$value->name ??''}}</strong>
                    @if($value->admin_id > '0')
                    <span>Quản trị viên</span>
                    @endif
                </div>
                <div class="item-content">
                    <p>{{$value->content ??''}}</p>
                </div>
                <div class="item-time">
                    <p>{!! change_time_to_text($value->created_at) !!}</p>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    @endif
    @endforeach
</div>