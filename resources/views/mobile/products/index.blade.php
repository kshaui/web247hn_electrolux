@extends('mobile.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "web247hn thiết kế web chuyên nghiệp",
        "logo": "{{config('app.url')}}/public/assets/img/logo.png"
    }
</script>
@endsection
@section('content')
  
<div class="container">
    <div class="cateProduct">
        <div class="cateProduct-right">
            <div class="cateProduct-right__title">
                <p>Bộ lọc</p>
            </div>
            <div class="cateProduct-right__list">
            @if(isset($product_categories) && count($product_categories) > 0)
                <div class="item">
                    <div class="item-title">
                        <p>Danh mục</p>
                    </div>
                    <div class="item-filter">
                        <div class="item-filter__cate">
                            @foreach($product_categories as $val)
                                <a @if( isset($slug) && $slug == $val->slug) class="active" @endif href="{{ $val->getUrlMb() }}">{{ $val->getName() }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            
            <div class="item">
                <div class="item-title">
                    <p>Giá bán</p>
                </div>
                <form action="" method="get" id="form-filter" >
                    <div class="item-filter">
                        @foreach(config('app.filter_price') as $key => $value)
                            <div class="item-filter__check">
                                <input type="radio" @if($filter_price == $key) checked="checked" @endif   value="{!! $key !!}" name="price_filter">
                                <span>
                                    {!! $value !!}
                                </span>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
               
            
            </div>
        </div>
        <div class="cateProduct-left">
            <div class="cateProduct-left__list">
                @include('mobile.products.item-product',['data'=>$data])
            </div>
        </div>
    </div>
</div>

@endsection