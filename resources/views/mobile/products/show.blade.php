@extends('mobile.layouts.app')
@section('head')
    <link rel="stylesheet" href="/assets/libs/hc-of-canvasnav/demo.css">
    <script src="/assets/libs/hc-of-canvasnav/hc-offcanvas-nav.js"></script>
    <link rel="stylesheet" href="/assets/libs/slick/slick.css">
    <link rel="stylesheet" href="/assets/libs/slick/slick-theme.css">
    <script src="/assets/libs/slick/slick.min.js"></script>
<script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org/",
                get"@type": "Product",
                "sku": "{{$product->id}}",
                "id": "{{$product->id}}",
                "mpn": "MobileCity",
                "name": "{{$product->name}}",
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($product->detail),150) : $meta_seo['description']}}",
                "image": "{{$product->getImage()}}",
                "brand": "{{$product_category->name}}",
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue": "{{@$rank_peren}}",
                    "reviewCount": "{{(@$rank_count==0)?1:@$rank_count}}"
                },
                "review": {
                    "@type": "Review",
                    "author": "Tran Van Thanh",
                    "reviewRating": {
                        "@type": "Rating",
                        "bestRating": "5",
                        "ratingValue": "1",
                        "worstRating": "1"
                    }
                },
                "offers": {
                    "@type": "AggregateOffer",
                    "priceCurrency": "VND",
                    "offerCount": 10,
                    "ee": "{{(isset($product->price))?($product->price):0}}",
                    "lowPrice":"{{(isset($product->price))?($product->price):0}}",
                    "highPrice":"{{(isset($product->price_old))?$product->price_old:0}}",
                    "priceValidUntil": "2019-12-31",
                    "availability": "http://schema.org/InStock",
                    "warranty": {
                        "@type": "WarrantyPromise",
                        "durationOfWarranty": {
                            "@type": "QuantitativeValue",
                            "value": "6 tháng",
                            "unitCode": "ANN"
                        }
                    },
                    "itemCondition": "mới",
                    "seller": {
                        "@type": "Organization",
                        "name": "web247hn"
                    }
                }
            },
            {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "web247hn",
                "url": "{{config('app.url')}}"
            }
        ]
    }
    </script>

@endsection
@section('content')
<div class="container">
	<div class="detailProduct default">
		<div class="detailProduct-top">
			<div class="detailProduct-top__left">
				<div class="slider-productTop">
                    <img src="{{ $product->getImage('large') }}">
                    @if($slides != '')
                        @foreach($slides as $v)
                            <img src="{{ $v }}">
                        @endforeach
                    @endif
				</div>
				<div class="slider-productBottom">
                    <img src="{{ $product->getImage('large') }}">
                    @if($slides != '')
                        @foreach($slides as $v)
                            <img src="{{ $v }}">
                        @endforeach
                    @endif
				</div>
			</div>
			<div class="detailProduct-top__right">
				<div class="product-name">
					<h1>{{ $product->getName() }}</h1>
				</div>
				<div class="product-info">
					<div class="product-info__sku">
						<p>Mã sp: {{ $product->sku??'Đang cập nhập' }}</p>
					</div>
					<div class="product-info__star">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
					</div>
				</div>
				<div class="product-price">
					<div class="product-price__new">
						<p>{{ $product->getPrice() }}</p>
					</div>
                    <div class="product-price__old">
                        <p>{{ $product->getPriceOld() }}</p>
                    </div>
				</div>
                @php 
                    $description = explode(PHP_EOL, $product->description??'');
                @endphp

               <!--  <div class="product-information">
                    <p class="title">Thông tin cơ bản</p>
                    <ul>
                        @foreach($description as $des)
                            <li>
                                <i class="fa fa-circle"></i> {!!$des!!}
                            </li>
                        @endforeach
                    </ul>
                </div> -->

                @if($gifts != '')
                <div class="promotion">
                    <p class="title">Khuyến mãi độc quyền</p>
                    <div class="info_promotion">
                        <ul>
                            @foreach($gifts as $key=>$value)
                                <li>
                                    <i>{{ $key+1 }}</i>
                                    {!! $value??'' !!}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif

                <div class="order">
                    <div href="javascript:;" data-id="{!! $product->id !!}" data-type="products" class="add_to_cart item" data-token="{!! csrf_token() !!}">
                        <i class="fa fa-shopping-cart"></i> 
                       <ul>
                            <li> Đặt hàng</li>
                            <li>(Không chờ đợi)</li>
                       </ul>
                    </div>
                    <div href="" class="item">
                        <img src="/assets/img/icon/vacuum-cleaner.png" alt="">
                        <ul>
                            <li><a href=""> Nhắn tin qua facebook</a></li>
                            <li><a href="">(Hỗ trợ 247)</a></li>
                        </ul>
                    </div>
                </div>


			</div>
		</div>
		<div class="detailProduct-bottom">
			<div class="detailProduct-bottom__title">
				 <p>Thông tin sản phẩm</p>
			</div>
			<div class="detailProduct-bottom__detail css-content">
                {!! $product->detail !!}
            </div>
		</div>
        @include('mobile.comments.item_cmt',['name'=>'products','post_data'=>$product])
		<div class="detailProduct-related">
			<div class="detailProduct-related__title">
				<p>Sản phẩm liên quan</p>
			</div>
			<div class="detailProduct-related__list">
                @if(isset($related_product) && count($related_product) > 0)
                    @foreach($related_product as $value)
                        <div class="item">
                            <div class="item-img">
                                <a href="{{ $value->getUrlMb() }}">
                                    <img src="{{ $value->getImage('medium') }}">
                                </a>
                            </div>
                            <div class="item-name">
                                <h3>
                                    <a href="{{ $value->getUrlMb() }}">
                                        {{ $value->getName() }}
                                    </a>
                                </h3>
                            </div>
                            <div class="item-fix">
                                <div class="item-fix__star">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                            <div class="item-price">
                                <div class="item-price__new">
                                    <span>{{ $value->getPrice() }}</span>
                                </div>
                                <div class="item-price__old">
                                    <span>{{ $value->getPriceOld() }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
				@endif
			</div>
		</div>
	</div>
</div>


@endsection

@section('foot')

<script type="text/javascript" src="/assets/js/slick.js"></script>

@endsection