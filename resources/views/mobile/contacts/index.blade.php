@extends('web.layouts.app')
@section('content')
<div class="container">
	
	<div class="contact">
		<div class="contact-left">
			<div class="contact-left__adress">
			 	<p>Lầu 9 tòa nhà AB , 76 Lê Lai , Phường Bến Thành , Quận 1 , TP HCM</p>
			 	<p>8:00 -> 18:00 ( thứ 2 - 7 )</p>
			</div>
			<div class="contact-left__infor">
			 	<p>Hotline: <span>1800 588 899 ( miễn phí cước gọi )</span></p>
			 	<p>Fax: <span>0283 588 899</span></p>
			 	<p>Email: <span>Vncare@elextrolux@gmail.com</span></p>
			</div>
		</div>
		<div class="contact-right">
			<div class="contact-right__form">
				<form>
					<div class="item-import">
						<input type="text" name="" placeholder="* Họ tên">
						<input type="text" name="" placeholder="* Email">
						<input type="text" name="" placeholder="* Số điện thoại">
						<textarea placeholder="* Nội dung "></textarea>
					</div>
					<div class="item-btn">
						<button>Gửi ngay <i class="fa fa-angle-right" aria-hidden="true"></i></button>
					</div>
				</form> 
			</div>
		</div>
	</div>
</div>
@endsection