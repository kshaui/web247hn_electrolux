@extends('web.layouts.app')
@section('head')
<link rel="stylesheet" type="text/css" href="/public/assets/css/home.css"/>
<link rel="stylesheet" type="text/css" href="/public/assets/css/base.css"/>
<link rel="stylesheet" type="text/css" href="/public/assets/css/news.css?v=1"/>
<style>
	.error-404{
		width: 100%;
		float: left;
		background: #fff;
		padding: 30px 0;
	}
	.error-404 .left{
		width: 40%;
		float: left;
	}
	.error-404 .left img{
		float: right;
		height: 200px;
	}
	.error-404 .right{
		width: 55%;
		float: left;
		margin-left: 5%;
	}
	.error-404 .p1{
		color: #333;
	    font-size: 20px;
	    font-weight: bold;
	    padding-bottom: 20px;
	}
	.error-404 .p2{
		color: #333;
		font-size: 16px;
		padding-bottom: 20px;
	}
	.error-404 .right input[type='text']{
		width: 270px;
		height: 35px;
		padding-left: 10px;
		border: 1px solid #ddd;
		border-radius: 4px;
		font-size: 16px;
	}.error-404 .right input[type='submit']{
		background: #0275b4;
	    color: #fff;
	    text-transform: uppercase;
	    text-align: center;
	    border: none;
	    padding: 11px 20px;
	    border-radius: 0 2px 2px 0;
	    position: relative;
	    bottom: 1px;
	    right: 16px;
	    cursor: pointer;
	}
	.bottom {
	    width: 100%;
	    background: #fff;
	    margin-bottom: 20px;
	}
	.titlerelate {
	    border-top: none;
	    padding: 30px 10px 10px;
	}
	.newsrelate li {
	    width: 22%;
	}
	.newsrelate li:nth-child(3n+1) {
	    clear: unset; 
	}
	.newsrelate li:nth-child(4n+1) {
	     clear: both; 
	}
</style>
@endsection
@section('content')
	<section id="container">
        <div class="wrap">
        	<div class="breadcrumb">
	        	<ol vocab="https://schema.org/" typeof="BreadcrumbList">
	        		<li property="itemListElement" typeof="ListItem"> 
	        			<a property="item" typeof="WebPage" href="/">
	        				<span property="name">Trang chủ</span>
	        			</a> 
	        			<meta property="position" content="1">
	        		</li> / 
	        		<li property="itemListElement" typeof="ListItem"> 
	        			<a property="item" typeof="WebPage"> 
	        				<span property="name">Không tìm thấy trang</span>
	        			</a> 
	        			<meta property="position" content="2"> 
	        		</li>
	        	</ol>
			</div>
        	<div class="error-404">
        		<div class="left">
        			<img src="/public/assets/img/image-404.png" alt="">
        		</div>
        		<div class="right">
        			<p class="p1">Xin lỗi, Trang bạn đang xem không tồn tại :(</p>
        			<p class="p2">Hãy tìm kiếm nội dung, dịch vụ, sản phẩm bạn cần: </p>
        			<form method="GET" action="/tim-kiem">
		                <input type="text" name="keyword" id="keyword" value="" autocomplete="off" placeholder="Nhập từ khóa tại đây ...">
		                <input type="submit" value="Tìm kiếm">
		            </form>
        		</div>
        	</div>
        	<div class="bottom">
                <h5 class="titlerelate">Bài viết mới</h5>
                <ul class="newsrelate">
                    @foreach($new_news as $value)
                    <li>
                        <a href="{!! $value->getRewrite() !!}" class="linkimg">
                            <div class="tempvideo">
                                <img src="{!! $value->getImage('x240') !!}" alt="{{$value->name}}">
                            </div>
                            <h3>{{$value->name}}</h3>
                            <span class="timepost">{{getTextTime($value->updated_at)}}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
			<div id="customer-request" class="clearfix">
                <div id="services-finder">
                    <div class="title">Không tìm thấy dịch vụ bạn cần?</div>
                    <div class="services-finder-form">
                        <div class="sff-gender-wrap">
                            <label><input type="radio" name="gender" value="1" class="sff-gender"> Anh</label>
                            <label><input type="radio" name="gender" value="2" class="sff-gender"> Chị</label>
                        </div>
                        <div class="clear"></div>
                        <div class="sff-left">
                            <input type="text" name="name" class="sff-name" placeholder="Họ và tên">
                            <input type="text" name="phone" class="sff-phone" placeholder="Số điện thoại">
                        </div>
                        <div class="sff-right">
                            <input type="text" name="service" class="sff-service" placeholder="Dịch vụ cần hỗ trợ">
                            <button class="sff-button">
                                <span class="sff-button-text-1">Gọi lại cho tôi</span>
                                <span class="sff-button-text-2">(Tôi cần hỗ trợ)</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div id="guarantee-check">
                    <div class="title">Tìm kiếm</div>
                    
                    <div class="guarantee-check-form clearfix">
                        <form action="{{route('web.search.index')}}" method="POST" role="form">
                            {{ csrf_field() }}
                    
                            <input class="gcf-info" type="text" required="" name="info" placeholder="Số điện thoại hoặc IMEI">
                            <input class="gcf-submit" type="submit" name="submit" value="Kiểm tra">
                        </form>
                    </div>
                    <div class="note">Ghi chú: Số IMEI ghi trên hóa đơn (5 số cuối của máy) <a href="
">Xem hướng dẫn</a> </div>
                </div>
            </div>
        </div>
    </section>
@endsection