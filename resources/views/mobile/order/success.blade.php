@extends('mobile.layouts.app')
@section('content')
<section class="category">
	<div class="container">	
		<div class="menu">
			<ul>
				<li><a href="/"> Trang chủ </a></li> 
				<li><a href="#"> Đặt hàng thành công </a></li> 
			</ul>  
		</div>	
		<div class="clear"></div>
		<div class="cart">
			<div class="cart-success">
				<p>Quý khách đã đặt hàng thành công!</p>
				<p>Sản phẩm sẽ được chuyển đến địa chỉ của quý khách sau 3 - 4 ngày tính từ thời điểm này.</p>
				<p>Nhân viên giao hàng sẽ liên hệ với quý khách trong vòng 24 tiếng.</p>
				<p>Cảm ơn quý khách đã sử dụng sản phẩm của chúng tôi . Xin cảm ơn!</p>
				<a href="{{ route('mobile.home') }}"><button class="cart-success__home">Quay về trang chủ</button></a>
			</div>
		</div>
		<div class="clear"></div>
	</div> 
</section>
@endsection