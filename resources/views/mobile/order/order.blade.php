@extends('mobile.layouts.app')
@section('content')
<section class="category">
	<div class="container">	
		<div class="menu">
			<ul>
				<i class="fa fa-home" aria-hidden="true"></i>
				<li><a href="/">Trang chủ</a></li>
				<li><a href="">Đặt hàng</a></li>
			</ul>
		</div>
		@if(count(Cart::content()) > 0)
		<div class="cart">
			<div class="cart-left">
				<div class="cart-left__info">
					<p class="cart-left__info-title">Thông tin đặt hàng</p>
					<form action="" id="create_cart" method="post" role="form">
						@csrf
						<div class="cart-left__info-form">
							<input type="text" placeholder="Họ tên" name="name"><br>
						</div>
						<div class="cart-left__info-form">
							<input type="text" placeholder="Số điện thoại" name="phone"><br>
						</div>
						<div class="cart-left__info-form">
							<input type="hidden" name="locale" value="vi">
							<input type="text" placeholder="Địa chỉ" name="adress"><br>
						</div>
						<div class="cart-left__info-form">
							<input type="text" placeholder="Email" name="email"><br>
							<input type="hidden" name="total_price" value="<?php echo Cart::total(); ?>">
						</div>
						<div class="cart-left__info-form">
							<select id='province_id' name ='provincial' required>
								<option value="">Chọn tỉnh/Thành phố</option>
								@foreach($province as $key => $value)
								<option name="provincial" value="{!!$value->id!!}" >{!!$value->name!!}</option>
								@endforeach
							</select>
							<select id='district_id' name ='district'>
								<option value="">Chọn quận/huyện</option>
							</select>
						</div>
						<div class="cart-left__info-form">
							<textarea name="notes" placeholder="Ghi chú"></textarea>
						</div>
						
					</form>
				</div>
				<div class="cart-left__payment">
					<div class="cart-left__payment-item">
						<p class="cart-left__payment-item__title">Vận chuyển</p>
						<div class="cart-left__payment-item__formInline">
							<p><i class="fa fa-check-circle" aria-hidden="true"></i> Giao hàng tận nơi <span>0 VNĐ</span></p>
						</div>
						<p class="cart-left__payment-item__title">Hình thức thanh toán</p>
						<div class="cart-left__payment-item__formInline">
							<p><i class="fa fa-check-circle" aria-hidden="true"></i> Thanh toán khi giao hàng (COD)</p>
							<p class="last-child">Bạn chỉ phải thanh toán khi nhận được hàng</p>
						</div>
					</div>
				</div>
			</div>
			<div class="cart-right">
				<p class="cart-left__payment-item__title">Đơn hàng ( <span><?php echo Cart::count(); ?></span> sản phẩm)</p>
				<?php foreach(Cart::content() as $row) :?>
				<div class="cart-right__content">
					<div class="cart-right__content-item">
						<div class="cart-right__content-item__img">
							<img src="<?php echo ($row->options->has('image') ? $row->options->image : ''); ?>">
							<span class="qty"><?php echo $row->qty; ?></span>
						</div>
						<div class="cart-right__content-item__name">
							<?php echo $row->name; ?>
						</div>
						<div class="cart-right__content-item__price">
							
							<?php echo number_format($row->price) ?> VNĐ
						</div>
					</div>
				</div>
				<?php endforeach;?>
				<div class="cart-right__fee">
					<p>Phí tạm tính: <span><?php echo Cart::total(); ?> VNĐ</span></p>
					<p>Phí vận chuyển: <span>0 VNĐ</span></p>
				</div>
				<p class="cart-right__total">Tổng cộng: <span> <?php echo Cart::total(); ?> VND</span></p>
				<div class="cart-right__btnaction">
					<a href=""><i class="fa fa-angle-left" aria-hidden="true"></i> Quay về giỏ hàng</a>

					{{-- href="{{route('cart.success" --}}
					
					<a name="btn" type="button" class="order" id="putCart" >Đặt hàng</a>	
				</div>
			</div>
		</div>
		@else
		<div class="cart-default">
			<div class="empty_cart" style="display:block">
				<h2 class="cart-default__title">Bạn chưa đặt sản phẩm nào !</h2>
				<div class="cart-default__home">
					<a href="/">Tiếp tục mua sắm</a>
				</div>
			</div>
		</div>
		@endif
	</div> 
</section>
@endsection