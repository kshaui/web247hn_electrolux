@extends('mobile.layouts.app')
@section('content')
<div class="container">
	
	<div class="support">
		<div class="support-top">
			@foreach($config_home['conf_service']['text_1'] as $key=>$value)
				<div class="support-top__item">
					<a href="{!! $config_home['conf_service']['text_3'][$key] !!}">
						<div class="item-icon">
							<span class="material-icons">
								{!! $config_home['conf_service']['text_2'][$key] !!}
							</span>
						</div>
						<div class="item-title">
							<p>{{ $value }}</p>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
	<div class="service">
		<div class="service-infor">
			<div class="support-title">
				<p>Trung tâm chăm sóc khách hàng</p>
			</div>
			<div class="service-infor__item">
				<div class="contact-left__adress">
				 	<p>{{$config_general['address']}}</p>
				</div>
				<div class="contact-left__infor">
				 	<p>Hotline: <span>{{$config_general['hotline']}}</span></p>
				 	<p>Fax: <span>{{$config_general['fax']}}</span></p>
				 	<p>Email: <span>{{$config_general['email']}}</span></p>
				</div>
			</div>
		</div>
		<div class="service-inspired">
			<div class="support-title">
				<p>Nguồn cảm hứng từ sản phẩm của elextrolux</p>
			</div>
			<div class="service-inspired__list">
				@foreach($news as $val)
				<div class="item">
					<div class="item-img">
						<a href="{{$val->getUrl()}}">
							<img src="{{$val->getImage('medium')}}">
						</a>
					</div>
					<div class="item-infor">
						<div class="item-infor__time">
							<p>{{$val->created_at??''}}</p>
						</div>
						<div class="">
							<p>{{$val->getDesc()}}</p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
<div class="support-slogan">
	<div class="support-title">
		<p>Chúng tôi luôn sẵn sàng hỗ trợ bạn</p>
	</div>
	<div class="container">
		@foreach($config_home['conf_service_sp']['text_1'] as $key=>$value)
		<div class="support-slogan__item">
			<div class="item-icon">
				<span class="material-icons">
					{!! $config_home['conf_service_sp']['text_2'][$key] !!}
				</span>
			</div>
			<div class="item-info">
				<div class="item-info__title">
					<p>{!! $config_home['conf_service_sp']['text_1'][$key] !!}</p>
				</div>
				<div class="item-info__btn">
					<p>{!! $config_home['conf_service_sp']['text_3'][$key] !!}</p>
					<a href="{!! $config_home['conf_service_sp']['text_5'][$key] !!}">{!! $config_home['conf_service_sp']['text_4'][$key] !!}</a>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection