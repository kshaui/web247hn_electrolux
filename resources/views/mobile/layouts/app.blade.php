<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{$config_general['image_favicon']}}" type="image/x-icon" rel="shortcut icon"/>       
    <script src="/assets/libs/jquery/jquery_341.min.js"></script>
    <link rel="stylesheet" href="/assetsMobile/css/main.css?v=1.0">
    <link rel="stylesheet" href="/assetsMobile/libs/font-awesome.css">
    <link rel="stylesheet" href="/assetsMobile/libs/materialicon.css">
    <link rel="stylesheet" href="/assetsMobile/libs/hc-of-canvasnav/demo.css">
    <script src="/assetsMobile/libs/hc-of-canvasnav/hc-offcanvas-nav.js"></script>
  
    
    @yield('head')
    @include('mobile.layouts.seo')
  
    {!! @$config_general['html_head'] !!}

    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "ProfessionalService",
          "image": [
            "https://laptop247hn.com/uploads/2019/11/laptop247hn.png",
            "https://laptop247hn.com/uploads/2019/11/laptop247hnfooter.png ",
            "https://laptop247hn.com/uploads/2019/11/sua-chua-laptop247hn.PNG"
           ],
          "@id": "https://laptop247hn.com",
          "name": "Cong ty TNHH Laptop247hn",
          "logo": "https://www.facebook.com/sualaptop247giare/",
          "priceRange": "$",
          "address": {
            "@type": "PostalAddress",
            "streetAddress": "???ng xuan ph??ng, ph??ng ph??ng canh, qu?n nam t? liem , ha n?i",
            "addressLocality": "Ha Noi",
            "addressRegion": "Vietnam",
            "postalCode": "10000",
            "addressCountry": "VN"
          },
          "geo": {
            "@type": "GeoCoordinates",
            "latitude": 13.290403,
            "longitude": 108.426511
          },
          "url": "https://laptop247hn.com",
            "sameAs": [
            "https://www.facebook.com/sualaptop247giare/",
            "https://www.youtube.com/channel/UCqayr_yD4W4Ef75IfvLG_7Q?view_as=subscriber"
          ],
          "telephone": "+84866628426",
          "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                  "Monday",
                  "Tuesday",
                  "Wednesday",
                  "Thursday",
              "Friday"
                ],
                "opens": "08:00",
                "closes": "17:30"
            },
            {
              "@type": "OpeningHoursSpecification",
              "dayOfWeek": "Saturday",
              "opens": "08:00",
              "closes": "12:00"
            }
          ]
        }
        </script>
            <script type="application/ld+json">
                {
                  "@context": "https://schema.org",
                  "@type": "WebSite",
                  "url": "https://laptop247hn.com/",
                  "potentialAction": {
                    "@type": "SearchAction",
                    "target": "https://laptop247hn.com/tim-kiem?keyword={search_term_string}",
                    "query-input": "required name=search_term_string"
                  }
                }
        
            </script>
           <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "Organization",
          "url": "https://laptop247hn.com/",
          "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+84-866628426",
            "contactType": "customer service",
            "contactOption": "HearingImpairedSupported",
            "areaServed": "VN"
          },{
            "@type": "ContactPoint",
            "telephone": "+84-343274132",
            "contactType": "sales"
          },{
            "@type": "ContactPoint",
            "telephone": "+84-866628426",
            "contactType": "technical support",
            "contactOption": [
              "HearingImpairedSupported"
            ],
            "areaServed": "VN"
          }
            ]
          }]
        }
        </script>

</head>
<body>
    {!! @$config_general['html_body'] !!}
    @include('mobile.layouts.adminbar')
    @include('mobile.layouts.header')
    @yield('slider')
    @include('mobile.layouts.breadcrumb')
    @yield('content')
    @include('mobile.layouts.footer')
    <div id="loading_box"><div id="loading_image"></div></div>
    @yield('foot')
</body>
</html>
<script type="text/javascript" src="/assetsMobile/js/script.js?v=1.0"></script>