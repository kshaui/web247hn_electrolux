<div class="inspiration">
    <div class="slider-item">
        <img src="{!! @$config_general['image_news_1'] !!}">
        <div class="container">
            <div class="item">
                <p>{!! @$config_general['title_news_1'] !!}</p>
                <span>
                    {!! @$config_general['des_news_1'] !!}
                </span>
            </div>
        </div>
    </div>
</div>