<div class="footer">
    <div class="footer-top">
        <div class="container">
            @if(isset($menu_footer))
            @for($i = 1; $i < count($menu_footer) + 1; $i++)
            @foreach($menu_footer as $value)
            
                <div class="footer-top__item">
                    <div class="item-title">
                        <a id="btn_activeFotter_{{$i}}">
                            <p>{{ $value->name }} <i class="fa fa-chevron-down"></i></p>
                        </a>
                    </div>
                    <div class="item-menu" id="form_activeFotter_{{$i++}}">
                        <ul>
                            @foreach($value->children as $children)
                            <li><a href="{{ $children->link }}">{{ $children->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            
            @endforeach
            @endfor
            @endif
        </div>
    </div>
</div>
<div class="content-container">
    <div class="container">
        <div class="elux-life-app small-12 large-4">
            <div class="content-container--table">
                <div class="block-table">
                    <strong>Ứng dụng Electrolux Life</strong>  
                </div>
                <div class="block-table">
                    <div class="vertical-container block-table-cell">
                            <a href="https://apps.apple.com/sg/app/electrolux-life/id1352924780">
                                <img class="loaded"  src="https://www.electrolux.vn/Static/assets/images/elux-life-app/_thumb_10779.png" alt="Download Electrolux Life App iPhone">
                            </a>
                    </div>
                    <div class="vertical-container block-table-cell">
                            <a href="https://play.google.com/store/apps/details?id=com.electrolux.electroluxlife">
                                <img class="loaded"  src="https://www.electrolux.vn/Static/assets/images/elux-life-app/_thumb_10778.png" alt="Download Electrolux App Android">
                            </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-share">
            <ul>
                <li>
                    <a href="{!!$config_general['link_facebook']!!}"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a href="{!!$config_general['link_pinterest']!!}"><i class="fa fa-pinterest-square"></i></a>
                </li>
                <li>
                    <a href="{!!$config_general['link_youtube']!!}"><i class="fa fa-youtube-play"></i></a>
                </li>
                <li>
                    <a href="{!!$config_general['link_twiter']!!}"><i class="fa fa-twitter"></i></a>
                </li>
            </ul>
            <div class="copyright">
                <span>
                    Coppyright © 2020 by elextrolux | Tư vấn và phát triển bởi: WEB247HN
                </span>
            </div>
        </div>
    </div>
</div>
<script>
    @if(isset($menu_footer))
        @for($i = 1; $i < count($menu_footer) + 1; $i++)
            $("#btn_activeFotter_{{$i}}").click(function(){
                $("#form_activeFotter_{{$i}}").slideToggle();
            })
        @endfor
    @endif
</script>