@php
    $menu_left = json_decode(@$config_general['menu_primarys'], true, 5) ;
    
@endphp

<header class="header">
    <div class="container header-bottom">
        <div class="header-bottom__left">
            <a href="/">
                <img src="assetsMobile/img/product/logo.png">
            </a>
        </div>
        <div class="header-bottom__right">
            <a href="{{route('mobile.cart.show')}}">
                <div class="item-cart">
                    <span class="material-icons">
                        shopping_cart
                    </span>
                </div>
                <div class="item-poin">
                    <span id="countCart-header"><?php echo Cart::count(); ?></span>
                </div>
            </a>
            <div id="main-nav" class="item-list">
                <span class="toggle material-icons">
                    reorder
                </span>
                <div id="main-nav" class="sidenav">
                    <ul class="first-nav">
                        <li data-nav-close="false" ></li>
                        <li class="cryptocurrency">
                            @if(isset($menu_left) && $menu_left !='')
                              @foreach($menu_left as $value)
                                <li>
                                  <a href="{{$value['link']??''}}">{{$value['name']??''}}</a>
                                  <?php
                                    $menu_child = $value['children']??'';
                                  ?>
                                  @if(isset($menu_child) && $menu_child !='')
                                  @foreach($menu_child as $v)
                                  <ul class="childmenu">
                                    <li><a href="{{$v['link']??''}}">{{$v['name']??''}}</a></li>
                                  </ul>
                                  @endforeach
                                  @endif
                                </li>
                              @endforeach
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header-bottom__search">
            <form action="{{ route('mobile.search.index') }}" method="get" id="searchform" role="search">
                {!! csrf_field() !!}
                <input type="text" autocomplete="off" class="search_inp" name="search" placeholder="Nhập nội dung tìm kiếm">
                <button type="reset" class="close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <button class="search"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="header-bottom__line"></div>
    </div>
</header>