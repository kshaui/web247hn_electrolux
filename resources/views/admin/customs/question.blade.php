@php
	$service = isset($value['value']) ? $value['value']: '';
    $service_question_maps = [];
	if(!empty($service)){
		$service_question_maps = DB::table('service_question_maps')->where('service_id', $service->id)->get();
	}
@endphp
<div class="form-group" style="padding-bottom: 20px;">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Câu hỏi thường gặp</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12" style="">
    	<div class="row">
            <div class="col-md-12 form-group">
                <a href="javascript:;" id="add-colors" class="btn btn-info">+ Thêm câu hỏi</a>
            </div>
        </div>
        @foreach($service_question_maps as $key => $value)
        	
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-12 form-group">
                    <input type="text" name="name_q[]" value="{{ $value->name }}" placeholder="">
                </div>
                <div class="col-md-12 form-group">
                    <textarea name="detail_q[]">{{ $value->detail }}</textarea>
                </div>
		       
		        <div class="col-md-12 form-group">
		            <a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>
		        </div>
		    </div>
	    @endforeach
    </div>
</div>
<script>
	function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
	$('#add-colors').on('click', function(){
		var rand = getRandomInt(1,999);
		var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-12 form-group">' +
                '<input id="" class="form-control" type="" name="name_q[]" value="">' +
               
                '</div>' +


                '<div class="col-md-12 form-group">' +
                
                '<textarea name="detail_q[]" class="form-control"></textarea>'+
               
                '</div>' +

               

                '<div class="col-md-12 form-group">' +
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>' +
                '</div>' +
                '</div>';
        var btn_group = $(this).closest('.controls');
        btn_group.append(str);
	});
	$('body').on('click','.remove-color',function () {
        $(this).closest('.row').remove();
    });
	$('body').on('change','.choose-color',function () {
        var code = $('option:selected', this).attr('data-code');
		var btn_group = $(this).closest('.row');
		$(btn_group).find('.color-demo').css('background', code);
    });
</script>