
<div class="form-group">
	@if ($full == 'false')
	<label class="control-label col-md-2 col-sm-2 col-xs-12"> {{__($label??'Box')}}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
	@else 
	<div class="controls col-md-12 col-sm-12 col-xs-12">
	@endif
		<table class="module_table_option has_image" border="1">
			<thead>
				<tr>
					@if ($full == 'false')
						<th colspan="4"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@else
						<td colspan="3"  style="padding: 0 10px;">{{__($label??'Box')}}</td>
						<th style="width: 30px;"><button type="button" class="btn-add add_{{$slug}}"><i class="fa fa-plus"></i></button></th>
					@endif
				</tr>
			</thead>
			<tbody>
				@if (isset($data[$name]['text_1']) && count($data[$name]['text_1']) > 0)
					@for ($i = 0; $i < count($data[$name]['text_1']); $i++)
						<tr>			
							<td>
								<input type="text" name="{{$name}}[text_1][]" class="form-control" placeholder="{{$placeholder_input_1??''}}" value="{{$data[$name]['text_1'][$i]??''}}">
								<input type="text" name="{{$name}}[text_2][]" class="form-control" placeholder="{{$placeholder_input_2??''}}" value="{{$data[$name]['text_2'][$i]??''}}">
								<input type="text" name="{{$name}}[text_3][]" class="form-control" placeholder="{{$placeholder_input_3??''}}" value="{{$data[$name]['text_3'][$i]??''}}">
								<input type="text" name="{{$name}}[text_4][]" class="form-control" placeholder="{{$placeholder_input_4??''}}" value="{{$data[$name]['text_4'][$i]??''}}">
								<input type="text" name="{{$name}}[text_5][]" class="form-control" placeholder="{{$placeholder_input_5??''}}" value="{{$data[$name]['text_5'][$i]??''}}">
							</td>
							<td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
						</tr>
					@endfor
				@endif

			</tbody>
		</table>
	</div>
</div>

<script>
$(document).ready(function() {
	$('body').on('click','.add_{{$slug}}',function() {
		{{$slug}}_number = $('.{{$slug}}_pc_image').length;
		template_{{$slug}} = `
			<td>
				<input type="text" name="{{$name}}[text_1][]" class="form-control" placeholder="{{$placeholder_input_1??''}}">
				<input type="text" name="{{$name}}[text_2][]" class="form-control" placeholder="{{$placeholder_input_2??''}}">
				<input type="text" name="{{$name}}[text_3][]" class="form-control" placeholder="{{$placeholder_input_3??''}}">
				<input type="text" name="{{$name}}[text_4][]" class="form-control" placeholder="{{$placeholder_input_4??''}}">
				<input type="text" name="{{$name}}[text_5][]" class="form-control" placeholder="{{$placeholder_input_5??''}}">
			</td>
		`;
		$(this).closest('.module_table_option').children('tbody').append(`
            <tr draggable="true">
                ${ template_{{$slug}} }
                <td><button type="button" class="btn-delete delete_option"><i class="fa fa-trash-o"></i></button></td>
            </tr>
        `);
		$('.module_table_option tbody').sortable();
	});
});
</script>