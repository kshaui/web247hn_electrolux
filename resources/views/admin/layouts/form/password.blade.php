{{-- 
	@include('admin.layouts.form.password',[
		'name' => 'text',
		'value' => 'text',
		'title' => 'text',
		'placeholder' => 'text',
		'required' => 1,
	])
 --}}
<div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12">
      	<input class="form-control" type="password" name="{!! $name??'' !!}" id="{!! $name??'' !!}" value="{!! $value??'' !!}" placeholder="{!! $placeholder??'' !!}">
    </div>
</div>