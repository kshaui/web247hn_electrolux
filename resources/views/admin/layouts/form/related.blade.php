{{-- 
	@include('admin.layouts.form.related',[
		'name' => 'text',
		'value' => 'text',
		'title' => 'text',
		'required' => 1,
		'placeholder' => 'text',
		'relate_table' => 'text',
		'relate_id' => 'text',
		'relate_name' => 'text',
	])
 --}}
<div class="form-group form-relate" data-name="{!! $name??'' !!}">
	<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<input class="form-control relate-search" type="text" name="search_{!! $name??'' !!}" id="{!! $name??'' !!}" value="" placeholder="{!! $placeholder??'' !!}" data-table="{!! $relate_table??'' !!}" data-id="{!! $relate_id??'' !!}" data-name="{!! $relate_name !!}">
		<div class="clear"></div>
		<ul class="relate-suggest"></ul>
		<div class="clear"></div>
		<div class="relate-result">
			@php
				if ($value != '') {
					$relate_list = DB::table($relate_table)->whereIn($relate_id,explode(',',$value))->get();
					if ($relate_list->count()) {
						foreach ($relate_list as $relate) {
						$relate = (array)$relate;
			@endphp
			<p class="relate-item">
				<input type="hidden" name="{!! $name??'' !!}[]" value="{!! $relate[$relate_id]??'' !!}">
				{!! $relate[$relate_name] !!}
				<a href="javascript:;" class="relate-item-remove"><i class="fa fa-times"></i></a>
			</p>
			@php
				}
			}
		}
			@endphp
		</div>
	</div>
</div>