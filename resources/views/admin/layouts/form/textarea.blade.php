{{-- 
	@include('admin.layouts.form.textarea',[
		'name' => 'text',
		'value' => 'text',
		'title' => 'text',
		'required' => 1,
		'placeholder' => 'text',
	])
 --}}
 <div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12">
	    <textarea rows="6" name="{!! $name??'' !!}" id="{!! $name??'' !!}" class="form-control" placeholder="{!! $placeholder??'' !!}">{!! $value??'' !!}</textarea>
    </div>
 </div>