<div class="alert alert-info" data-toggle="collapse" data-target="#{{$name}}">
	<strong>{{__($title??'')}}</strong>
</div>
<div id="{{$name}}" class="collapse" style="height: auto;">
	
	@include('admin.layouts.form.table.1_image_2_input_1_textarea',[
		'full' => 'false',
		'name' => $name,
		'slug' => $name,
		'value' => $data[$name]??'',
		'label' => 'Danh sách bình luận',
		'placeholder_title' => 'Tên người đánh giá',
		'placeholder_subtitle' => 'Địa chỉ người đánh giá',
		'placeholder_area' => 'Nội dung đánh giá',
	])
	
	@php $banner_text = 'banner_text'; @endphp
	
</div>