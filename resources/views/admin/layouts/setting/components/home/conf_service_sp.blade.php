<div class="alert alert-info" data-toggle="collapse" data-target="#{{$name}}">
	<strong>{{__($title??'')}}</strong>
</div>
<div id="{{$name}}" class="collapse" style="height: auto;">
	
	@include('admin.layouts.form.table.5_input',[
		'full' => 'false',
		'name' => $name,
		'slug' => $name,
		'value' => $data[$name]??'',
		'label' => 'Danh sách dịch vụ',
		'placeholder_input_1' => 'Tên tiêu đề',
		'placeholder_input_2' => 'Icon',
		'placeholder_input_3' => 'Miêu tả ngắn',
		'placeholder_input_4' => 'Tên button hiển thị',
		'placeholder_input_5' => 'Url',
	])
	
	@php $banner_text = 'banner_text'; @endphp
	
</div>