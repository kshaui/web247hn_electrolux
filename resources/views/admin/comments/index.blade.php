<td style="width: 200px;">
    <select name="status" class="form-control edit_quick" data-table="{{$table}}" style="margin-bottom:10px;">
        @foreach($arr_stt as $k => $v)
        <option value="{{$k}}" @if($value->status == $k) selected="" @endif">{{$v}}</option>
        @endforeach
    </select>
    <p>
        @php
            switch ($value->type) {
                case 'products': {
                    foreach ($products as $product) {
                        if ($product->id == $value->type_id) {
                            @endphp
                                {{-- @if($is_mobile)
                                <p><a target="_blank" href="{{$product->getUrlMb()}}">{{$product->name}}</a></p>
                                @else --}}
                                  <p><a target="_blank" href="{{$product->getUrl()}}">{{$product->name}}</a></p>
                                {{-- @endif --}}
                            @php
                        }
                    }
                }
                break;
                case 'news': {
                    foreach ($news as $new) {
                        if ($new->id == $value->type_id) {
                            @endphp
                                {{-- @if($is_mobile)
                                <p><a target="_blank" href="{{$new->getUrlMb()}}">{{$new->name}}</a></p>
                                @else --}}
                                  <p><a target="_blank" href="{{$new->getUrl()}}">{{$new->name}}</a></p>
                               {{--  @endif --}}
                            @php
                        }
                    }
                }
                break;
            }
        @endphp
    </p>
</td>
<td class="comment_content">   
    <p>{!!nl2br($value->content)!!}</p>
    @foreach ($comment_admins as $comment_admin)
        @if ($comment_admin->parent_id == $value->id)
            <p class="comment_admin">
                <strong>{{$comment_admin->name??'Admin vô danh'}}:</strong> 
                {{$comment_admin->content}}
                <span class="comment_admin_show_more"><i class="fa fa-plus"></i></span>
            </p>
        @endif
    @endforeach
</td>
<td>
    <p>Tên: {!!$value->name??'Không xác định'!!}</p>
    <p>Điện thoại: {!!$value->phone??'Không xác định'!!}</p>
    <p>Email: {!!$value->email??'Không xác định'!!}</p>
</td>
<td style="width:110px;">
    <p>Số sao: {!!$value->rank!!}<i class="fa fa-star"></i></p>
    <p><strong>Thêm lúc:</strong> {!! $value->created_at !!}</p>    
    <p><strong>Cập nhật:</strong> {!! $value->updated_at !!}</p>
</td>
<td style="width:50px;">
    <a href="{!! route('comments.reply',['id'=>$value->id]) !!}" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Trả lời</a>
    <a class="btn btn-success btn-xs" href='#modal-comment'>Trả lời ngay</a>
</td>