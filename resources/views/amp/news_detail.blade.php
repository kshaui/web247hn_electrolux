@extends('amp.layouts.app')

@section('content')
    <section role="main" class="ui-content jqm-content">
        <div class="wrap">
            <h1 id="news-name">{{$news->name}}</h1>
          
            <div style="clear: both;"></div>
            <div class="infomation-news">
                <ul>
                 



                    <li>{!! date('d/m/Y', strtotime($news->created_at)) !!}</li>
                    <li><i class="fa fa-comments-o" aria-hidden="true"></i></li>
                    
                </ul>
            </div>
            <div id="news-image">
                @php
                    list($width, $height) = getimagesize($news->getImage());
                @endphp
                <amp-img layout = "responsive" width="{!! $width !!}" height="{!! $height !!}" src="{!! $news->getImage() !!}" alt="{{$news->name}}" title="{{$news->name}}"></amp-img>
            </div>
            <div id="news-content">

                <div class="css-content">
                    {{-- <amp-youtube data-videoid="RkepnIZzGYc" layout="responsive" width="480" height="270"></amp-youtube> --}}
                    @php
                        $news_detail = $news->detail;
                        $str_relate_news = '';
                        if(isset($relate_news) && $relate_news != null) {
                            $end_p_pos = strpos($news_detail,'</p>');
                            if($end_p_pos !== false) {
                                $end_p_pos += 4;
                                $news_detail_start = substr($news_detail,0,$end_p_pos);
                                $news_detail_end = substr($news_detail,$end_p_pos);
                                $news_detail = $news_detail_start.$str_relate_news.$news_detail_end;
                            }
                        }
                    @endphp
                    @php
                        $news_detail = removeIframe($news_detail);
                        $news_detail = getImageSize1($news_detail);
                        $news_detail = str_replace('type=""', '', $news_detail);
                        $news_detail = str_replace('spellcheck="false"', '', $news_detail);
                        $news_detail = str_replace('<img', '<amp-img layout = "responsive"', $news_detail);
                        $news_detail = str_replace('<iframe', '<amp-iframe width="200" height="100" sandbox="allow-scripts allow-same-origin" layout = "responsive"', $news_detail);
                        $news_detail = str_replace('/iframe', '/amp-iframe', $news_detail);
                    @endphp
                    {!! filterShortCode($news_detail, 1) !!}
                    @if(isset($relate_news) && $relate_news != null)
                    <p class="relate-news-detail-title">Bài viết liên quan</p>
                    <ul class="relate-news-detail-list">
                        @foreach ($relate_news as $value)
                        @php
                            list($width, $height) = getimagesize($value->getImage());
                        @endphp
                        <li>
                            <div class="img">
                                <a href="{!! $value->getRewrite() !!}"><amp-img layout = "responsive" width="{!! $width !!}" height="{!! $height !!}" src="{!! $value->getImage("x120") !!}" alt="{!! $value->name !!}"></amp-img></a> 
                            </div>
                            <a class="name" href="{!! $value->getRewrite() !!}">{!! $value->name !!}</a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
                <div id="detail-comment">
                    <div id="detail-comment-tab" class="clearfix">
                        <div class="detail-comment-title">Hỏi đáp</div>
                        <div class="detail-rating clearfix" id="review">
                            
                        </div>
                    </div>
                    <div id="detail-comment-main">
                        <div id="sudo_comments">
                            <div id="comment_list">
                                @if($comments && count($comments))
                                    @foreach($comments as $value)
                                        <div class="comment_item comment_item_parent" id="comment_item_{{$value['comment']->id}}">
                                            <div class="comment_item_author">
                                                <div class="comment_item_author_avatext">{{getCharacterAvatar($value['comment']->name)}}</div>
                                                <span class="comment_item_author_name">{{$value['comment']->name}}</span>
                                                @if($value['comment']->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                                            </div>
                                            <div class="comment_item_content css-content">{!! nl2br($value['comment']->content) !!}</div>
                                            @if($value['comment']->images)
                                            @php
                                            $images = explode(',',$value['comment']->images);
                                            @endphp
                                            <div class="comment_item_image">
                                                @foreach($images as $img)
                                                <a class="comment_item_image_link" href="{!!$img!!}"><amp-img src="{!!image_by_link($img,'x80')!!}" width="80" height="80"></amp-img></a>
                                                @endforeach
                                            </div>
                                            @endif
                                            <div class="comment_item_meta">
                                                <span class="comment_item_time">{{getSimpleTextTime($value['comment']->created_at)}}</span>
                                            </div>
                                            @if(count($value['child']))
                                            <div class="comment_item_reply_wrap @if(count($value['child'])) has_child @endif">
                                                @if(count($value['child']))
                                                    @foreach($value['child'] as $v)
                                                        <div class="comment_item comment_item_child" id="comment_item_{{$v->id}}">
                                                            <div class="comment_item_author">
                                                                <div class="comment_item_author_avatext">{{getCharacterAvatar($v->name)}}</div>
                                                                <span class="comment_item_author_name">{{$v->name}}</span>
                                                                @if($v->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                                                            </div>
                                                            <div class="comment_item_content css-content">{!! strip_tags(nl2br($v->content),'<br><a><img>') !!}</div>
                                                            @if($v->images)
                                                            @php
                                                            $images = explode(',',$v->images);
                                                            @endphp
                                                            <div class="comment_item_image">
                                                                @foreach($images as $img)
                                                                <a class="comment_item_image_link" href="{!!$img!!}"><amp-img src="{!!image_by_link($img,'x80')!!}" width="80" height="80"></amp-img></a>
                                                                @endforeach
                                                            </div>
                                                            @endif
                                                            <div class="comment_item_meta">
                                                                <span class="comment_item_time">{{getSimpleTextTime($v->created_at)}}</span>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                            @else
                                                <div class="comment_item_reply_wrap"></div>
                                            @endif
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="news-box">
                <div class="news-box-title">Có thể bạn quan tâm</div>
                <div class="news-box-main">
                    <ul class="relate-news-list">
                        @foreach($new_news as $value)
                            @php
                                list($width, $height) = getimagesize($value->getImage());
                            @endphp
                            <li>
                                <div class="img"><a href="{!! $value->getRewrite() !!}"><amp-img layout = "responsive" width="{!! $width !!}" height="{!! $height !!}" src="{!! $value->getImage('x120') !!}" alt="{{$value->name}}"></div>
                                <a class="name" href="{!! $value->getRewrite() !!}">{{$value->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection