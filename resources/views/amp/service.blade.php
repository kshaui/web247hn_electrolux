@extends('amp.layouts.app')
@section('head')
    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Product",
            "sku": "{{$service->id}}",
            "id": "{{$service->id}}",
            "name": "{{$service->name}}",
            "description": "{{cutString(removeHTML($service->detail),170)}}",
            "image": "{!! $service->getImage() !!}",
            "brand": "{{$category->brand}}",
            "mpn": "Thành Trung",
            "review": {
                "@type": "Review",
                "author": "Tu Cao",
                "reviewRating": {
                    "@type": "Rating",
                    "bestRating": "5",
                    "ratingValue": "1",
                    "worstRating": "1"
                }
            },
            "offers": {
                "@type": "Offer",
                "url": "{!! config('app.url').$service->getRewrite() !!}",
                "priceCurrency": "VND",
                "price": "{{$service->price != '' ? $service->price : '0'}}",
                @if(date('m') < 6)
                "priceValidUntil": "{{date('Y')}}-06-1",
                @else
                "priceValidUntil": "{{date('Y')}}-12-31",
                @endif
                "availability": "http://schema.org/{{$service->instock != 2 ? 'InStock' : 'OutOfStock'}}",
                "warranty": {
                    "@type":"WarrantyPromise",
                    "durationOfWarranty":{
                    "@type":"QuantitativeValue",
                    "value":"6 tháng",
                    "unitCode":"ANN"
                }
            },
            "itemCondition":"mới",
            "seller": {
                "@type": "Organization",
                "name": "ThanhTrungMobile"
            }
          }
        }
    </script>
@endsection
@section('content')
    <div id="header-amp">
        <div class="icon-prev">
            <a href="/"><amp-img src="/public/amp/img/icon-prev.png" width="28" height="21"></a>
        </div>
        <div class="detail-name">
            <h1>{{$service->name}}</h1>
        </div>
        <div class="icon-bars">
            <a href=""><amp-img src="/public/amp/img/image-bars.png" width="28" height="24"></a>
        </div>
        <input id="check01" style="right: 8px;left: unset;" type="checkbox" name="menu"/>
        <ul class="submenu">
                @foreach($menu_data as $value)
                @php
                    $current_category = $value->categories;
                @endphp
                <li>
                    <a href="{!! $current_category->getRewrite() !!}">{{$current_category->name}}</a>
                </li>
            @endforeach
            <li><a href="/tin-tuc">Tin công nghệ</a></li>
            <li><a href="/lien-he">Liên hệ</a></li>
        </ul>
        <div id="navbar-overlay"></div>
    </div>
    <section role="main" style="clear: both;padding-top: 20px;background: #fff;" class="ui-content jqm-content">
        <div class="wrap">
            <div id="detail-image">
                <amp-img src="{!! $service->getImage() !!}" width="300" height="300" style="margin-left: calc((100% - 300px) / 2);" alt="{{$service->name}}"></amp-img>
                @if($service->price != '')
                <p style="font-size: 18px; font-weight: bold; color: #d9021b; padding-top: 15px;">{{format_price($service->price)}}</p>
                @endif
                <ul class="detail-note">
                    <p style="padding-bottom: 10px;"><b>Thời gian khắc phục:</b></p>
                    <li><b>Thay màn:</b> Từ 30 - 60 phút (tùy độ khó)</li>
                    <li><b>Thay kính:</b> Từ 2 - 3 tiếng (tùy độ khó)</li>
                    <li><b>Phần cứng:</b> Từ 1 - 3 ngày (tùy độ khó)</li>
                </ul>
            </div>
            <div id="detail-price">
                <h3 class="title">Giá dịch vụ</h3>
                @php
                    if($service->price_table == '') {
                        $price_table_string = '<p class="price-one">'.format_price($service->price).'</p>' ;
                    }else {
                        //Xử lý price table đối với bản cũ là dữ liệu json
                        if(isJson($service->price_table)) {
                            $price_table = json_decode($service->price_table,true);
                            $price_table_string = '<table>
                            <tr>
                                <th>STT</th>
                                <th>Tên linh kiện</th>
                                <th>Giá</th>
                                <th>Bảo hành</th>
                            </tr>';
                            foreach($price_table as $k=>$v) {
                                $price_table_string .= '<tr>
                                    <td>'.($k+1).'</td>
                                    <td>'.$v['name'].'</td>
                                    <td>'.$v['price'].'</td>
                                    <td>'.$v['warranty'].'</td>
                                </tr>';
                            }
                            $price_table_string .= '</table>';
                        }else {
                            $price_table_string = $service->price_table;
                            $price_table_string = getTablePirce($price_table_string);
                        }
                    }
                @endphp
                {!! $price_table_string !!}
                <p style="padding: 10px;" class="detail-price-note content-location-option">
                    Bạn đang xem tại chi nhánh 
                    @if(isset($_COOKIE['location']))
                        <b>{{$array_locations[$_COOKIE['location']]}}</b>&nbsp;
                        (Bạn ở khu vực khác, hãy
                        <select class="location-option">
                            <option>Thay đổi địa điểm</option>
                            @foreach($array_locations as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>)
                    @endif
                    <br>
                    Lưu ý: Giá áp dụng cho khách lẻ, chưa có công tháo lắp và bảo hành
                </p>
            </div>
            <div class="call-me clearfix">
                <div class="call-me-title">Hãy để lại số điện thoại, chúng tôi sẽ gọi cho bạn ngay !</div>
                <div class="call-me-main">
                    <input type="text" name="call-me-phone" class="call-me-phone" placeholder="Nhập số điện thoại của bạn">
                    <button class="call-me-btn">Gọi lại cho tôi</button>
                </div>
            </div>
            <div id="detail-guide">
                <div class="detail-guide-hotline content-location-option">
                    @if(isset($_COOKIE['location']))
                        {{$array_locations[$_COOKIE['location']]}}:&nbsp;
                        <a href="tel:{!! str_replace(['.','-',' '],'',$current_store->hotline) !!}">{!! $current_store->hotline !!}</a>&nbsp;
                        <select class="location-option" style="width: 80px;">
                            <option>Thay đổi</option>
                            @foreach($array_locations as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <div style="padding: 10px;">
                    @php
                        $count_stores = 0;
                        @endphp
                        @foreach($locations as $value)
                            <div class="detail-address-group detail-address-group-{{$value->id}}"@if(isset($_COOKIE['location']) && $_COOKIE['location'] == $value->id){!! ' style="display: block;"' !!}@endif>
                                @php
                                $stores = $value->stores()->where('status',1)->orderBy('order','ASC')->get();
                                @endphp
                                @if($stores->count())
                                    @foreach ($stores as $v)
                                    @php
                                    $count_stores++;
                                    @endphp
                                        <div class="detail-address-item">
                                            <p class="name">{!! $v->address !!}</p>
                                            <p class="hotline"><a href="tel:{!! getStringTel($v->hotline) !!}">{!! $v->hotline !!}</a> &nbsp; <a style="font-weight: normal;color: #288ad6;" href="{!! $v->maps !!}" target="_blank"><i class="fa fa-location-arrow"></i> Chỉ đường</a> </p>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        @endforeach
                </div>
                <div class="detail-guide-hotline">
                    Tổng đài <a href="tel:19000360">1900.0360</a>
                </div>
            </div>
            <div id="detail-content">
                <div class="title">Bài viết, đánh giá</div>
                <div id="detail-content-main" class="main css-content">
                    <div class="clear"></div>
                    <div id="detail-content-title">Thông tin dịch vụ</div>
                    <div id="detail-content-text" class="css-content">
                        @php
                            $service_detail = $service->detail;
                            // $service_detail = removeWidth($service_detail);
                            // $service_detail = removeHeight($service_detail);
                            $service_detail = str_replace('<img', '<amp-img layout = "responsive"', $service_detail);
                            $service_detail = str_replace('<iframe', '<amp-iframe layout = "responsive"', $service_detail);
                        @endphp
                        {!! filterShortCode($service_detail, 1) !!}
                    </div>
                    @if(count($tags))
                        <div class="tags-list" style="padding: 15px 10px;">
                            <span><i class="fa fa-tags"></i> Tags:&nbsp;&nbsp;</span>
                            @foreach($tags as $tag)
                                <a href="{!! $tag->getRewrite() !!}">{{$tag->name}}</a>&nbsp;&nbsp;
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div id="detail-comment">
                <div id="detail-comment-tab" class="clearfix">
                    <div class="detail-comment-title">Hỏi đáp</div>
                    <div class="detail-rating clearfix" id="review">
                        @if($rating->count > 0)
                            @php
                                $avg = round($rating->avg * 2) / 2;
                            @endphp
                            <div class="rating-show" style="padding-left: 10px;">
                                <div class="rating-show-start">
                                    @for($i = 0; $i < 5; $i++)
                                        @if($i < $avg && $i + 0.5 == $avg)
                                            <amp-img src="/public/amp/img/fa-start-half-o.PNG" width="15" height="15" style="margin-top: 16px;" alt=""></amp-img>
                                        @elseif($i < $avg)
                                            <amp-img src="/public/amp/img/fa-start.PNG" width="15" height="15" style="margin-top: 16px;" alt=""></amp-img>
                                        @else
                                            <amp-img src="/public/amp/img/fa-start-o.PNG" width="15" height="15" style="margin-top: 16px;" alt=""></amp-img>
                                        @endif
                                    @endfor
                                </div>
                                <a class="rating-show-count" href="#review">
                                    <span>{{$avg}}</span> / <span>{{$rating->count}}</span> đánh giá
                                </a>
                            </div>
                            <div class="kksr-legend" style="display: none;">
                                <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                    <div itemprop="name" class="kksr-title hidden">{!! $service->name !!}</div>
                                    <span itemprop="ratingValue">{{$avg}}</span><span itemprop="ratingCount">{{$rating->count}}</span> đánh giá    
                                    <meta itemprop="bestRating" content="5">
                                    <meta itemprop="worstRating" content="1">
                                    <div itemprop="itemReviewed" itemscope="" itemtype="http://schema.org/CreativeWork"></div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div id="detail-comment-main">
                    <div id="sudo_comments">
                        <div id="comment_list">
                            @if($comments && count($comments))
                                @foreach($comments as $value)
                                    <div class="comment_item comment_item_parent" id="comment_item_{{$value['comment']->id}}">
                                        <div class="comment_item_author">
                                            <div class="comment_item_author_avatext">{{getCharacterAvatar($value['comment']->name)}}</div>
                                            <span class="comment_item_author_name">{{$value['comment']->name}}</span>
                                            @if($value['comment']->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                                        </div>
                                        <div class="comment_item_content css-content">{!! nl2br($value['comment']->content) !!}</div>
                                        @if($value['comment']->images)
                                        @php
                                        $images = explode(',',$value['comment']->images);
                                        @endphp
                                        <div class="comment_item_image">
                                            @foreach($images as $img)
                                            <a class="comment_item_image_link" href="{!!$img!!}"><amp-img src="{!!image_by_link($img,'x80')!!}" width="80" height="80"></amp-img></a>
                                            @endforeach
                                        </div>
                                        @endif
                                        <div class="comment_item_meta">
                                            <span class="comment_item_time">{{getSimpleTextTime($value['comment']->created_at)}}</span>
                                        </div>
                                        @if(count($value['child']))
                                        <div class="comment_item_reply_wrap @if(count($value['child'])) has_child @endif">
                                            @if(count($value['child']))
                                                @foreach($value['child'] as $v)
                                                    <div class="comment_item comment_item_child" id="comment_item_{{$v->id}}">
                                                        <div class="comment_item_author">
                                                            <div class="comment_item_author_avatext">{{getCharacterAvatar($v->name)}}</div>
                                                            <span class="comment_item_author_name">{{$v->name}}</span>
                                                            @if($v->admin_id > 0) <span class="comment_item_author_admin">Quản trị viên</span> @endif
                                                        </div>
                                                        <div class="comment_item_content css-content">{!! strip_tags(nl2br($v->content),'<br><a><img>') !!}</div>
                                                        @if($v->images)
                                                        @php
                                                        $images = explode(',',$v->images);
                                                        @endphp
                                                        <div class="comment_item_image">
                                                            @foreach($images as $img)
                                                            <a class="comment_item_image_link" href="{!!$img!!}"><amp-img src="{!!image_by_link($img,'x80')!!}" width="80" height="80"></amp-img></a>
                                                            @endforeach
                                                        </div>
                                                        @endif
                                                        <div class="comment_item_meta">
                                                            <span class="comment_item_time">{{getSimpleTextTime($v->created_at)}}</span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        @else
                                            <div class="comment_item_reply_wrap"></div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if($relate_services != null)
            <div id="detail-news-relate">
                <div class="title">Dịch vụ liên quan</div>
                <div class="main">
                    <ul class="news-relate-list">
                        @foreach($relate_services as $value)
                            <li>
                                <div class="img"><a href="{!! $value->getRewrite() !!}"><amp-img src="{!! $value->getImage('x120') !!}" width="120" height="120" alt="{{$value->name}}"/></amp-img></a> </div>
                                <a class="name" href="{!! $value->getRewrite() !!}">{{$value->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            @if($relate_news != null)
            <div id="detail-news-relate">
                <div class="title">Bài viết liên quan</div>
                <div class="main">
                    <ul class="news-relate-list">
                        @foreach($relate_news as $value)
                            <li>
                                <div class="img"><a href="{!! $value->getRewrite() !!}"><amp-img src="{!! $value->getImage('x120') !!}" width="120" height="120" alt="{{$value->name}}"/></amp-img></a> </div>
                                <a class="name" href="{!! $value->getRewrite() !!}">{{$value->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>
    </section>
    <div id="action-bottom">
        <a href="{{ isset($meta_seo['url']) ? $meta_seo['url'] : '' }}">
            <div class="item-action item-action-1">
                <amp-img width="29" height="24" src="/public/amp/img/icon-chat.png" alt=""></amp-img>
                <div style="padding-top: 4px;">Chat ngay</div>
            </div>
            <div class="item-action item-action-1">
                <amp-img width="28" height="28" src="/public/amp/img/icon-cart.png" alt=""></amp-img>
                <div>Thêm vào giỏ hàng</div>
            </div>
            <div class="item-action item-action-2">
                Mua ngay
            </div>
        </a>
    </div>
@endsection