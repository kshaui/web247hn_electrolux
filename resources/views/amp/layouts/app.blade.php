<!doctype html>
<html amp lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="{{url('/')}}/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    @include('web.layouts.seo')
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <style amp-custom>
        @php
            echo file_get_contents('https://thanhtrungmobile.vn/public/amp/css/base.css?v=3');
            echo file_get_contents('https://thanhtrungmobile.vn/public/amp/css/mobile.min.css?v=3');
            echo file_get_contents('https://thanhtrungmobile.vn/public/amp/css/hc-offcanvas-nav.css');
            echo file_get_contents('https://thanhtrungmobile.vn/public/amp/css/comments.css');

        @endphp
        .css-content table.sudo-content-img{
              width: 100%;
          }
          .css-content table.sudo-content-img td{
            border: none;
            padding: 0;
          }
          .sudo-content-img-caption p{
              text-align: center;
          }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>

    {{-- <script type="text/plain" src="https://thanhtrungmobile.vn/public/assets/js/jquery-3.1.1.min.js"></script> --}}
    {{-- <script type="text/plain" src="https://thanhtrungmobile.vn/public/amp/js/hc-offcanvas-nav.js"></script> --}}
    {{-- {!! isset($seo_setting['seo_headtag']) ? $seo_setting['seo_headtag'] : '' !!} --}}
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "ProfessionalService",
      "image": [
        "https://sudospaces.com/thanhtrungmobile-vn/2019/03/doi-ngu-nhan-vien-thanh-trung.jpg",
        "https://sudospaces.com/thanhtrungmobile-vn/2018/08/doi-ngu-nhan-vien-thanh-trung.jpg",
        "https://sudospaces.com/thanhtrungmobile-vn/2018/12/doi-ngu-ky-thuat-vien.jpg"
       ],
      "@id": "https://thanhtrungmobile.vn",
      "name": "Hệ thống sửa chữa điện thoại Thành Trung Mobile",
      "logo": "https://sudospaces.com/thanhtrungmobile-vn/2019/05/logo-thanh-trung-mobile.png",
      "priceRange": "$",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "1399 đường 3 tháng 2, Quận 11",
        "addressLocality": "Ho Chi Minh",
        "addressRegion": "Vietnam",
        "postalCode": "70000",
        "addressCountry": "VN"
      },
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": 21.050453,
        "longitude": 105.843862
      },
      "url": "https://thanhtrungmobile.vn",
        "sameAs": [
        "https://www.facebook.com/thanhtrungmobile.hcm/",
        "https://www.youtube.com/channel/UCyIsxznCjd5sI5N3hZPekrA",
        "https://twitter.com/thanhtrungmb"
      ],
      "telephone": "+8419000360",
      "openingHoursSpecification": [
        {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
              "Monday",
              "Tuesday",
              "Wednesday",
              "Thursday",
              "Friday"
            ],
            "opens": "08:00",
            "closes": "20:00"
        },
        {
          "@type": "OpeningHoursSpecification",
          "dayOfWeek": "Sunday",
          "opens": "08:30",
          "closes": "17:30"
        }
      ]
    }
    </script>
    @yield('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<amp-analytics type="gtag" data-credentials="include">
    <script type="application/json">
    {
      "vars" : {
        "gtag_id": "UA-48538157-4",
        "config" : {
          "UA-48538157-4": { "groups": "default" }
        }
      }
    }
    </script>
    </amp-analytics>
@include('amp.layouts.header')
@yield('content')
@include('amp.layouts.footer')
@yield('foot')
{{-- <script src="/public/assets/js/functions.min.js"></script> --}}
{{-- <script src="/public/amp/js/scripts.js"></script> --}}

</body>
</html>