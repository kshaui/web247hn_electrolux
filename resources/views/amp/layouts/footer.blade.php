<footer id="footer">
    <div class="wrap">
        <div class="footer-support ui-grid-a">
            <div class="footer-support-item ui-block-a">
                <p>Tư vấn (24/7)</p>
                @php
                    $hotline_1 = '1900.0360';
                    if(isset($content_setting['hotline_1'])) {
                        $hotline_1 = $content_setting['hotline_1'];
                    }
                    if(isset($_COOKIE['location']) && $_COOKIE['location'] != 1) {
                        $hotline_1 = $current_store->hotline;
                    }
                @endphp
                <a href="tel:{!! str_replace(['.','-',' '],'',$hotline_1) !!}"><i class="fa fa-phone"></i> {!! $hotline_1 !!}</a>
            </div>
            <div class="footer-support-item ui-block-b">
                <p>Góp ý, phản ánh</p>
                @php
                    $hotline_2 = '1900.0360';
                    if(isset($content_setting['hotline_2']))
                        $hotline_2 = $content_setting['hotline_2'];
                @endphp
                <a href="tel:{!! str_replace(['.','-',' '],'',$hotline_2) !!}"><i class="fa fa-phone"></i> {!! $hotline_2 !!}</a>
            </div>
        </div>
        <div class="footer-info">
            <div class="footer-info-tab ui-grid-a">
                <div class="footer-info-tab-item ui-block-a"><a href="https://thanhtrungmobile.vn/trang/gioi-thieu-ve-thanh-trung-mobile.html">Thông tin khác <i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
                <div class="footer-info-tab-item ui-block-b"><a href="https://thanhtrungmobile.vn/lien-he">Địa chỉ cửa hàng <i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
            </div>
            <div class="footer-info-main">
                <div class="footer-info-main-item item-1">
                    <div class="footer-info-item">
                        <div class="title">Thông tin</div>
                        <ul class="list-style">
                            <li><a href="/trang/gioi-thieu-ve-thanh-trung-mobile.html">Giới thiệu</a></li>
                            <li><a href="/trang/quy-che-hoat-dong.html">Quy chế hoạt động</a></li>
                            <li><a href="/lien-he">Liên hệ</a></li>
                            <li><a href="/tuyen-dung">Tuyển dụng</a></li>
                            <li><a href="/trang/hop-tac-kinh-doanh.html">Hợp Tác Kinh Doanh</a></li>
                        </ul>
                    </div>
                    <div class="footer-info-item">
                        <div class="title">Điều khoản &amp; Chính sách</div>
                        <ul class="list-style">
                            <li><a href="/trang/chinh-sach-bao-mat-thong-tin.html">Chính sách bảo mật</a></li>
                            <li><a href="/trang/chinh-sach-bao-hanh.html">Chính sách bảo hành</a></li>
                            <li><a href="/trang/chinh-sach-gia-re-nhat.html">Chính sách giá rẻ nhất</a></li>
                            <li><a href="/trang/chinh-sach-van-chuyen.html">Chính sách vận chuyển</a></li>
                        </ul>
                    </div>
                </div>
                <div class="footer-info-main-item item-2">
                    @foreach($locations as $value)
                        <div class="footer-contact-address-title">{!! $value->name !!}</div>
                        <ul class="footer-contact-address-list">
                            @php
                                $current_stores = $value->stores()->where('status',1)->orderBy('order','ASC')->get();
                            @endphp
                            @foreach ($current_stores as $v)
                                <li>{!! $v->address !!} - Hotline: <a href="tel:{!! getStringTel($v->hotline) !!}">{!! $v->hotline !!}</a></li>
                            @endforeach
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="footer-bottom" @if(isset($service)) style="padding-bottom: 80px;" @endif>
    <p>Bạn đang xem phiên bản rút gọn AMP</p>
    <p><a href="{{ isset($meta_seo['url']) ? $meta_seo['url'] : '' }}">Xem phiên bản đầy đủ</a></p>
</div>
{{-- <a class="site-hotline" href="tel:{!! str_replace(['.','-',' '],'',$hotline_1) !!}"><i class="fa fa-phone"></i> Gọi tư vấn</a> --}}
{{-- <div class="popup-overlay" onclick="closePopup();"></div> --}}