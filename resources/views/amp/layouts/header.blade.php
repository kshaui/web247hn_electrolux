@if(!isset($service))
<header id="header">
	<div class="wrap">
		<div class="main-header clearfix">
			<a id="navigation-mobile-toggle">
	            <i class="fa fa-bars"></i>
	            <amp-img src="/public/amp/img/icon-bars.png" width="30" height="30"></amp-img>
	        </a>
	        <input id="check01" type="checkbox" name="menu"/>
			<ul class="submenu">
			    @foreach($menu_data as $value)
			        @php
			            $current_category = $value->categories;
			            $current_child_classes = $value->child_classes;
			        @endphp
			        <li>
						<a href="{!! $current_category->getRewrite() !!}">{{$current_category->name}}</a>
						{{-- <ul>
				            @foreach($current_child_classes as $child_class)
				                <li><a href="{!! $child_class->getRewrite() !!}">{{$child_class->name}}</a></li>
				            @endforeach
				        </ul> --}}
			        </li>
			    @endforeach
	            @foreach($config_menu as $value)
	            @php
	                if(isset($value->children)){
	                    $config_menu_2 = $value->children;
	                }
	            @endphp
	            	@foreach($config_menu_2 as $val)
	                	<li>
	                		<a href="{!! $val->link !!}" @if($val->taget == 1) target="_blank" @endif @if($val->rel == 1) rel="nofollow" @endif>{{$val->name}}</a>
	                	</li>
	            	@endforeach
	            @endforeach
			    <li><a href="/tin-tuc">Tin công nghệ</a></li>
			    <li><a href="/lien-he">Liên hệ</a></li>
			</ul>
			<div id="navbar-overlay"></div>
	        <div class="logo">
	        	<a href="/"><amp-img src="/public/assets-mobile-v2/img/logo-mobile.png" width="210" height="24" style="margin-left: calc((100% - 210px)/2);" alt="Logo Thành Trung Mobile"></amp-img></a>
	        </div>
	        <div class="header-support">
	        	<span>Gọi tư vấn</span>
	        	<br>
	        	@php
                    $hotline_1 = '1900.0360';
                    if(isset($content_setting['hotline_1'])) {
                        $hotline_1 = $content_setting['hotline_1'];
                    }
                    if(isset($_COOKIE['location']) && $_COOKIE['location'] != 1) {
                        $hotline_1 = $current_store->hotline;
                    }
                @endphp
	        	<a href="tel:{!! str_replace(['.','-',' '],'',$hotline_1) !!}">{!! $hotline_1 !!}</a>
	        </div>
		</div>
		<div class="header-search clearfix">
            <button class="search-btn"><i class="fa fa-search"></i></button>
            <input type="text" name="keyword" id="keyword" autocomplete="off" placeholder="Bạn muốn sửa chữa dịch vụ gì?">
		</div>
	</div>
</header>
@endif