@extends('amp.layouts.app')
@section('head')
    
@endsection
@section('content')
    <section role="main" class="ui-content jqm-content">
        <div class="wrap">
            <h1 class="category-title">{{$category->name}}</h1>
            <div style="clear: both;"></div>
            @if(count($child_categories))
            <div class="category-menu">
                <div class="category-menu-item">
                    <a class="category-menu-item-title">Chọn dòng máy <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    <input id="check02" type="checkbox"/>
                    <ul class="category-menu-item-list">
                        @foreach($child_categories as $value)
                            <li><a href="{!! $value->getRewrite() !!}">{{$value->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @if(count($relate_classes))
                <div class="category-menu-item clearfix">
                    <a class="category-menu-item-title">Chọn loại dịch vụ <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    <input id="check03" type="checkbox"/>
                    <ul class="category-menu-item-list">
                        @foreach($relate_classes as $value)
                            <li><a href="{!! $value->getRewrite() !!}">{{$value->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            @endif
            <div class="clear"></div>
            <div class="category-list">
                <div class="category-list-main ui-grid-a">
                    @foreach($list_data as $key=>$value)
                        @php
                            try {
                                list($width, $height) = getimagesize($value->image);
                            } catch (Exception $e) {
                                $width = 300;
                                $height = 300;
                            }
                            
                        @endphp
                        <div class="product-list-item {{($key%2 == 0) ? 'ui-block-a' : 'ui-block-b'}}">
                            <div class="img">
                                <a href="{!! $value->getRewrite() !!}">
                                    <amp-img layout = "responsive" src="{!! $value->getImage('x150') !!}" width="{!! $width !!}" height="{!! $height !!}" alt="{!! $value->name !!}"></amp-img>
                                </a>
                            </div>
                            <h3 class="name"><a href="{!! $value->getRewrite() !!}">{!! $value->name !!}</a> </h3>
                            <p class="price"><a href="{!! $value->getRewrite() !!}">Xem chi tiết</a></p>
                        </div>
                    @endforeach
                </div>
                {!! $list_data->links() !!}
            </div>
            <div class="clear"></div>
            @if($category->detail)
            <div class="category-description show-more-wrap" style="padding: 10px; background: #fff; margin-top: 20px; text-align: left;">
                <div class="css-content">
                    @php
                        // $category_detail = removeWidth($category->detail);
                        // $category_detail = removeHeight($category_detail);
                        $category_detail = str_replace('<img', '<amp-img layout = "responsive"', $category->detail);
                        $category_detail = str_replace('<iframe', '<amp-iframe layout = "responsive"', $category_detail);
                    @endphp
                    {!! filterShortCode($category_detail, 1) !!}
                </div>
            </div>
            @endif
        </div>
    </section>
@endsection