@extends('web.layouts.app')
@section('content')
<section class="category">
	<div class="container">	
		<div class="menu">
			<ul>
				<i class="fa fa-home" aria-hidden="true"></i>
				<li><a href="/">Trang chủ</a></li>
				<li><a href="">Giỏ hàng</a></li>
			</ul>
		</div>
		@if(Cart::count() > 0)
		<div class="cart-table">
			<div class="cart" id="order">
				<table>
					<h3>Giỏ hàng</h3>
					<thead>
						<tr>
							<th>Ảnh sản phẩm</th>
							<th>Tên sản phẩm</th>
							<th>Số lượng</th>
							<th>Giá</th>
							<th>Thành tiền</th>
							<th>Xóa</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach(Cart::content() as $row) :?>
						
						<tr id="delete-cart-{!! $row->rowId !!}">
							@csrf
							<td><img src="<?php echo ($row->options->has('image') ? $row->options->image : ''); ?>"></td>
							<td>
								<p><strong><?php echo $row->name; ?></strong></p>
								<p><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></p>
							</td>
							<td>
								<button class="cart-btnMinus" type="button" data-soluong = "{!! $row->qty !!}" data-update="{!! $row->rowId !!}">-</button>
								<input type="text" min="1" disabled  class="cart-show"  value="<?php echo $row->qty; ?>">
								<button class="cart-btnPlus" type="button" data-update="{!! $row->rowId !!}">+</button>
							</td>
							<td><?php echo number_format("$row->price") ?> VNĐ </td>
							<td class="total_all">
								<span id="total_all"><?php echo number_format("$row->total") ?> VNĐ</span></td>
							<td>
								<button class="cart-btnRemove">
									<i class="material-icons" onclick="remove_cart('{!! $row->rowId !!}')">delete</i>  
								</button>
							</td>
						</tr>
						<?php endforeach;?>
						<tr>
							<td colspan="5" class="cart-total">Tổng tiền </td>
							<td>
								<span id="total"><?php echo Cart::total(); ?> VNĐ</span></td>
							</td>
						</tr>
					</tbody>
				</table>
				@if(Cart::count() > 0)
					<a href="{{ route('cart.add') }}"><button id="myBtn" class="cart-order">ĐẶT HÀNG</button></a>
					<a href="{{ URL::previous() }}"><button class="cart-contine">TIẾP TỤC MUA</button></a>
				@endif
			</div>
		</div>

		<div class="cartMob" id="order">
			<table>

				<h3>Giỏ hàng</h3>
				<thead>
					<tr>
						<th style="width: 35%">Ảnh sản phẩm</th>
						<th style="width: 50%">Thông tin</th>
						<th style="width: 15%">Xóa</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach(Cart::content() as $row) :?>
					
					<tr id="delete-cart-{!! $row->rowId !!}">
						<td><img src="<?php echo ($row->options->has('image') ? $row->options->image : ''); ?>">
						</td>
						<td>
							<p><strong><?php echo $row->name; ?></strong></p>
							<p><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></p>
							<button class="cartMob-btnMinus" type="button" data-soluong = "{!! $row->qty !!}" data-update="{!! $row->rowId !!}">-</button>
							<input type="text" min="1" disabled  class="cartMob-show"  value="<?php echo $row->qty; ?>">
							<button class="cartMob-btnPlus" type="button" data-update="{!! $row->rowId !!}">+</button>
							<p>Giá : <span><?php echo number_format("$row->price") ?> VNĐ </span></p>
						</td>
						<td>
							<button class="cart-btnRemove">
								<i class="material-icons" onclick="remove_cart('{!! $row->rowId !!}')">delete</i>  
							</button>
						</td>
					</tr>
					<?php endforeach;?>
					<tr>
						<td colspan="2" class="cart-total">Tổng tiền </td>
						<td>
							<span id="totals"><?php echo Cart::total(); ?> VNĐ</span></td>
						</td>
					</tr>
				</tbody>
			</table>
			@if(Cart::count() > 0)
				<a href="{{ route('cart.add') }}"><button id="myBtn" class="cart-order">ĐẶT HÀNG</button></a>
				<a href="/"><button class="cart-contine">TIẾP TỤC MUA</button></a>
			@endif
		</div>
		@else
		<div class="cart-default">
			<div class="empty_cart" style="display:block">
				<h2 class="cart-default__title">Không có sản phẩm nào trong giỏ hàng của bạn!</h2>
				<div class="cart-default__home">
					<a href="/">Tiếp tục mua sắm</a>
				</div>
			</div>
		</div>
		@endif
	</div> 
</section>
@endsection
