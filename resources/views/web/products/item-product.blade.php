@if(isset($data) && count($data) > 0)
@foreach($data as $value)
@if($value->description != null)
    @php
        $des_array = preg_split('/\n|\r\n/',$value->description);
    @endphp
@endif


<div class="item">
    <div class="item-img">
        <a href="{{ $value->getUrl() }}">
            <img src="{{ $value->getImage('medium') }}">
        </a>
    </div>
    <div class="item-name">
        <h3>
            <a href="{{ $value->getUrl() }}">
                {{ $value->getName() }}
            </a>
        </h3>
    </div>
    <div class="item-fix">
        <div class="item-fix__star">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
        </div>
    </div>
    <div class="item-price">
        <div class="item-price__new">
            <span>{{ $value->getPrice() }}</span>
        </div>
        <div class="item-price__old">
            <span>{{ $value->getPriceOld() }}</span>
        </div>
    </div>
</div>


@endforeach
@endif