<header class="header">
    <div class="header_top">
        <div class="container">
            <div class="header_top__item">
                <p><i class="fa fa-envelope"></i> {{$config_general['email']}}</p>
            </div>
            <div class="header_top__item">
                <p><i class="fa fa-phone"></i> {{$config_general['hotline']}}</p>
            </div>
            <div class="header_top__item">
                <p><i class="fa fa-map-marker"></i> {{$config_general['address']}}</p>
            </div>
        </div>
    </div>
    <div class="header_bottom">
        <div class="container">
            <div class="header_bottom__logo">
                <a href="/">
                    <img src="{{$config_general['logo_header']}}">
                </a>
            </div>
            <div class="header_bottom__menu">
                <ul>
                    @php
                        // menu
                        $menu_primary_top = json_decode(@$config_general['menu_primarys'], true, 6) ;
                    @endphp

                    @if(isset($menu_primary_top) && $menu_primary_top !='')
                    @foreach(array_slice($menu_primary_top, 0, 6) as $value)
                    <li><a href="{{$value['link']??''}}">{{$value['name']??''}}</a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
            <div class="header_bottom__search">
                <form action="{{ route('web.search.index') }}" method="get" id="searchform" role="search">
                {!! csrf_field() !!}
                    <input type="text" name="search" placeholder="Tìm kiếm">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
                
            </div>
            <div class="header_bottom__cart">
                <a href="{{route('cart.show')}}">
                    <i class="fa fa-shopping-cart"></i>
                    <span id="countCart-header"><?php echo Cart::count(); ?></span>
                </a>
            </div>
        </div>
    </div>
</header>