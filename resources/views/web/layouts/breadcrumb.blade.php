
@if (isset($breadcrumbs))
<div class="container">
    <div class="menu">
        <ul>
            <i class="fa fa-home" aria-hidden="true"></i>
            <li><a  href="/" title="">Trang chủ</a></li>
            @foreach($breadcrumbs as $key=>$value)
                <li><a  href="{{$value['url']}}" title="">{{$value['name']}} </a></li>
            @endforeach       
        </ul>
    </div>
</div>
@endif
  