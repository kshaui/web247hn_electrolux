@extends('web.layouts.app')
@section('content')
<div class="container">
	<div class="support">
		<div class="support-top">
			@foreach($config_home['conf_service']['text_1'] as $key=>$value)
				<div class="support-top__item">
					<a href="{!! $config_home['conf_service']['text_3'][$key] !!}">
						<div class="item-icon">
							<span class="material-icons">
								{!! $config_home['conf_service']['text_2'][$key] !!}
							</span>
						</div>
						<div class="item-title">
							<p>{{ $value }}</p>
						</div>
					</a>
				</div>
			@endforeach
		</div>
		<div class="support-title">
			<p>Dịch vụ sửa chữa</p>
		</div>
		
		<div class="support-detail css-content">
			{!! $service->detail !!}
		</div>

		<div class="support-cateVal" id="support-form">
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy hút mùi</a></li>
					<li><a href="">Bếp</a></li>
					<li><a href="">Lò nướng</a></li>
					<li><a href="">Lò vi sóng</a></li>
					<li><a href="">Tủ lạnh</a></li>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Tủ lạnh</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
		</div>
		<div class="support-cateVal" id="support-form1">
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy hút mùi</a></li>
					<li><a href="">Bếp</a></li>
					<li><a href="">Lò nướng</a></li>
					<li><a href="">Lò vi sóng</a></li>
					<li><a href="">Tủ lạnh</a></li>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Tủ lạnh</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
		</div>
		<div class="support-cateVal" id="support-form2">
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy hút mùi</a></li>
					<li><a href="">Bếp</a></li>
					<li><a href="">Lò nướng</a></li>
					<li><a href="">Lò vi sóng</a></li>
					<li><a href="">Tủ lạnh</a></li>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Tủ lạnh</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
		</div>
		
		<div class="accessories">
			<div class="accessories-left">
				<a href="">
					<img src="assets/img/product/pk.png">
				</a>
			</div>
			<div class="accessories-right">
				<div class="accessories-right__name">
					<a href="">
						<h3>Các phụ kiện và phụ tùng</h3>
					</a>
				</div>
				<div class="accessories-right__title">
					<p>Khám phá các phụ kiện và phụ tùng thay thế để giữ thiết bị của bạn luôn hoạt động tốt nhất.</p>
					<a href="">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>

		<div class="support-question">
			<div class="support-title">
				<p>Câu hỏi thường gặp</p>
			</div>
			<div class="support-question__list">
				@if(isset($service_question_maps) && count($service_question_maps) >0)
					@foreach($service_question_maps as $key=>$value)
						<div class="item">
							<a id="support-questionBtn{{ $key+1 }}">{!! $value->name !!}<i class="fa fa-plus" aria-hidden="true"></i></a>
							<div class="item-value" id="support-questionform{{ $key+1 }}">
								{!! $value->detail !!}
							</div>
						</div>
					@endforeach
				@endif
				
			</div>
		</div>

		<div class="service-inspired">
			<div class="support-title">
				<p>Nguồn cảm hứng từ sản phẩm của elextrolux</p>
			</div>
			<div class="service-inspired__list">
				 @include('web.news.item-news',['data'=>$news])
			</div>
		</div>
	</div>
</div>
<div class="support-slogan">
	<div class="support-title">
		<p>Chúng tôi luôn sẵn sàng hỗ trợ bạn</p>
	</div>
	<div class="container">
		@if(isset($config_home['conf_service_sp']['text_1']))
		@foreach($config_home['conf_service_sp']['text_1'] as $key=>$value)
		<div class="support-slogan__item">
			<div class="item-icon">
				<span class="material-icons">
					{!! $config_home['conf_service_sp']['text_2'][$key] !!}
				</span>
			</div>
			<div class="item-info">
				<div class="item-info__title">
					<p>{!! $config_home['conf_service_sp']['text_1'][$key] !!}</p>
				</div>
				<div class="item-info__btn">
					<p>{!! $config_home['conf_service_sp']['text_3'][$key] !!}</p>
					<a href="{!! $config_home['conf_service_sp']['text_5'][$key] !!}">{!! $config_home['conf_service_sp']['text_4'][$key] !!}</a>
				</div>
			</div>
		</div>
		@endforeach
		@endif
	</div>
</div>
@endsection