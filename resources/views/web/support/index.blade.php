@extends('web.layouts.app')
@section('content')
<div class="container">
	<div class="support">
		<div class="support-top">
			@foreach($config_home['conf_service']['text_1'] as $key=>$value)
				<div class="support-top__item">
					<a href="{!! $config_home['conf_service']['text_3'][$key] !!}">
						<div class="item-icon">
							<span class="material-icons">
								{!! $config_home['conf_service']['text_2'][$key] !!}
							</span>
						</div>
						<div class="item-title">
							<p>{{ $value }}</p>
						</div>
					</a>
				</div>
			@endforeach
		</div>

		<div class="support-title">
			<p>Tìm theo loại sản phẩm</p>
		</div>
		<div class="support-cate">
			@foreach($cate as $val)
			<div class="support-cate__item">
				<div class="item-img">
					<img src="{{$val->getImage('medium')}}">
				</div>
				<div class="item-cate">
					<a id="support-btnform"> {{$val->getName()}} <i class="fa fa-angle-down"></i></a>
				</div>
			</div>
			@endforeach
			@foreach($cate as $val)
			<div class="support-cateVal" id="support-form">
				<div class="support-cateVal__item">
					<ul>
						@foreach($cate_child as $value)
						@if($value->parent_id == $val->id)
						<li><a href="{{$value->getUrlSp()}}">{{$value->getName()}}</a></li>
						@endif
						@endforeach
					</ul>
				</div>
			</div>
			@endforeach
		</div>
		
		

		<div class="support-cateVal" id="support-form1">
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy hút mùi</a></li>
					<li><a href="">Bếp</a></li>
					<li><a href="">Lò nướng</a></li>
					<li><a href="">Lò vi sóng</a></li>
					<li><a href="">Tủ lạnh</a></li>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Tủ lạnh</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
		</div>
		<div class="support-cateVal" id="support-form2">
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy hút mùi</a></li>
					<li><a href="">Bếp</a></li>
					<li><a href="">Lò nướng</a></li>
					<li><a href="">Lò vi sóng</a></li>
					<li><a href="">Tủ lạnh</a></li>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Tủ lạnh</a></li>
				</ul>
			</div>
			<div class="support-cateVal__item">
				<ul>
					<li><a href="">Máy rửa bát</a></li>
				</ul>
			</div>
		</div>
		
		<div class="accessories">
			<div class="accessories-left">
				<a href="">
					<img src="assets/img/product/pk.png">
				</a>
			</div>
			<div class="accessories-right">
				<div class="accessories-right__name">
					<a href="">
						<h3>Các phụ kiện và phụ tùng</h3>
					</a>
				</div>
				<div class="accessories-right__title">
					<p>Khám phá các phụ kiện và phụ tùng thay thế để giữ thiết bị của bạn luôn hoạt động tốt nhất.</p>
					<a href="">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>

		<div class="support-question">
			<div class="support-title">
				<p>Câu hỏi thường gặp</p>
			</div>
			<div class="support-question__list">
				<div class="item">
					<a id="support-questionBtn1">Làm sao để tôi đăng ký bảo hành sản phẩm mới <i class="fa fa-plus" aria-hidden="true"></i></a>
					<div class="item-value" id="support-questionform1">
						<p>Đăng ký bảo hành sản phẩm để nhận những lời khuyên hữu ích về cách sử dụng thiết bị đúng cách. Chỉ cần đến phần "Đăng ký" và điền đầy đủ thông tin vào biểu mẫu đăng ký</p>
						<p>*Chú ý: Đăng ký bảo hành sản phẩm chỉ áp dụng qua đăng ký Online</p>
					</div>
				</div>
				<div class="item">
					<a id="support-questionBtn2">Làm sao để tôi đăng ký bảo hành sản phẩm mới <i class="fa fa-plus" aria-hidden="true"></i></a>
					<div class="item-value" id="support-questionform2">
						<p>Đăng ký bảo hành sản phẩm để nhận những lời khuyên hữu ích về cách sử dụng thiết bị đúng cách. Chỉ cần đến phần "Đăng ký" và điền đầy đủ thông tin vào biểu mẫu đăng ký</p>
						<p>*Chú ý: Đăng ký bảo hành sản phẩm chỉ áp dụng qua đăng ký Online</p>
					</div>
				</div>
				<div class="item">
					<a id="support-questionBtn3">Làm sao để tôi đăng ký bảo hành sản phẩm mới <i class="fa fa-plus" aria-hidden="true"></i></a>
					<div class="item-value" id="support-questionform3">
						<p>Đăng ký bảo hành sản phẩm để nhận những lời khuyên hữu ích về cách sử dụng thiết bị đúng cách. Chỉ cần đến phần "Đăng ký" và điền đầy đủ thông tin vào biểu mẫu đăng ký</p>
						<p>*Chú ý: Đăng ký bảo hành sản phẩm chỉ áp dụng qua đăng ký Online</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="support-slogan">
	<div class="support-title">
		<p>Chúng tôi luôn sẵn sàng hỗ trợ bạn</p>
	</div>
	<div class="container">
		<div class="support-slogan__item">
			<div class="item-icon">
				<span class="material-icons">
					insert_comment
				</span>
			</div>
			<div class="item-info">
				<div class="item-info__title">
					<p>Gửi tin nhắn</p>
				</div>
				<div class="item-info__btn">
					<p>Chia sẻ thắc mắc của bạn bất cứ lúc nào</p>
					<a href="">Gửi yêu cầu</a>
				</div>
			</div>
		</div>
		<div class="support-slogan__item">
			<div class="item-icon">
				<span class="material-icons">
					phone_iphone
				</span>
			</div>
			<div class="item-info">
				<div class="item-info__title">
					<p>Trao đổi trực tiếp</p>
				</div>
				<div class="item-info__btn">
					<p>Từ 9h - 17h , thứ 2 đến thứ 6</p>
					<a href="">+ 1900 588 899</a>
				</div>
			</div>
		</div>
		<div class="support-slogan__item">
			<div class="item-icon">
				<span class="material-icons">
					build
				</span>
			</div>
			<div class="item-info">
				<div class="item-info__title">
					<p>Sửa chữa</p>
				</div>
				<div class="item-info__btn">
					<p>Trung tâm bảo hành gần nhất</p>
					<a href="">Công cụ tìm kiếm</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection