@extends('web.layouts.app')
@section('head')
 	<link rel="stylesheet" href="/assets/libs/slick/slick.css">
    <link rel="stylesheet" href="/assets/libs/slick/slick-theme.css">
    <script src="/assets/libs/slick/slick.min.js"></script>
<script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "{{$news->getUrl()}}"
                },
                "headline": "{{$news->name}}",
                "image": {
                    "@type": "ImageObject",
                    "url": "{{$news->getImage()}}",
                    "height": 800,
                    "width": 800
                },
                 "aggregateRating": {
                    "@type": "AggregateRating",
	                 "ratingValue": "{{@$rank_peren}}",
	                 "reviewCount": "{{@$rank_count}}"
                },
                "datePublished": "{{date('Y/m/d',strtotime($news->created_at))}}",
                "dateModified": "{{date('Y/m/d',strtotime($news->updated_at))}}",
                "author": {
                    "@type": "Person",
                    "name": "{{$admin_name??'web247hn'}}"
                },
                "publisher": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}",
                    "logo": {
                        "@type": "ImageObject",
                        "url": "https://web247hn.com/themes/img/logo.png",
                        "width": 600,
                        "height": 60
                    }
                },
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($news->detail),150) : $meta_seo['description']}}"
            }, {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "{{config('app.name')}}",
                "url": "{{config('app.url')}}"
            },
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "{{config('app.url')}}",
                "@id": "{{config('app.url')}}/#organization",
                "name": "web247hn",
                "logo": "{{config('app.url')}}/public/assets/img/logo.png"
            }
        ]
    }




</script>
@endsection
@section('content')



<div class="container">
	<div class="inspired">
		<div class="inspired-left">
			<div class="inspired-left__title">
				<h1>{{ $news->getName() }}</h1>
				<p>{{ date('d/m/Y H:s A',strtotime($news->created_at)) }}</p>
			</div>
			<div class="css-content">
				{!! $news->detail !!}
			</div>
		</div>
		<div class="inspired-right">
			<div class="inspired-right__title">
				<p>Sản phẩm nổi bật</p>
			</div>

			@if(isset($hot_products) && count($hot_products) > 0)
				<div class="cateProduct-left__list">
					@foreach($hot_products as $value)
					 	<div class="item">
					 		<div class="item-img">
					 			<a href="{{ $value->getUrl() }}">
						 			<img src="{{ $value->getImage('medium') }}" alt="{{ $value->slug }}">
						 		</a>
					 		</div>
					 		<div class="item-name">
					 			<h3>
					 				<a href="{{ $value->getUrl() }}">
					 					{{ $value->getName() }}
					 				</a>
					 			</h3>
					 		</div>
					 		<div class="item-fix">
					 			<div class="item-fix__sku">
					 				<span>{{ $value->sku }}</span>
					 			</div>
					 			<div class="item-fix__star">
					 				<i class="fa fa-star"></i>
					 				<i class="fa fa-star"></i>
					 				<i class="fa fa-star"></i>
					 				<i class="fa fa-star"></i>
					 				<i class="fa fa-star"></i>
					 				(1)
					 			</div>
					 		</div>
					 		<div class="item-price">
					 			<div class="item-price__new">
					 				<span>{{ $value->getPrice() }}</span>
					 			</div>
					 			<div class="item-price__old">
					 				<span>{{ $value->getPriceOld() }}</span>
					 			</div>
					 		</div>
					 	</div>
				 	@endforeach
				</div>
			@endif
		</div>
	</div>
</div>	
  
@if(isset($related_news) && count($related_news) > 0)
	<div class="inspiredRelated">
		<div class="container">
			<div class="inspiredRelated-title">
				<p>Bài liên quan</p>
			</div>
			<div class="service-inspired">
				<div class="inspiredRelated-list service-inspired__list">
					@foreach($related_news as $value)
						<div class="item">
							<div class="item-img">
								<a href="{{ $value->getUrl() }}">
									<img src="{{ $value->getImage('medium') }}" alt="{{ $value->slug }}">
								</a>
							</div>
							<div class="item-infor">
								<div class="item-infor__time">
									<p>{{ date('d/m/Y H:a A',strtotime($value->created_at)) }}</p>
								</div>
								<div class="">
									<p>{{ $value->getName() }}</p>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endif

@endsection

@section('foot')

<script type="text/javascript" src="/assets/js/slick.js"></script>

@endsection