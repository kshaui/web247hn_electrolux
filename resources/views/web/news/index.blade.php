@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "web247hn thiết kế web chuyên nghiệp",
        "logo": "{{config('app.url')}}/public/assets/img/logo.png"
    }
</script>
@endsection
@section('slider')
 @include('web.layouts.slider')
@endsection
@section('content')
<div class="inspiration">
    <div class="container">
        <div class="inspiration-cate">
            <div class="inspiration-cate__btn">
                @if(isset($news_categories) && count($news_categories) >0 )
                    @foreach($news_categories as $value)
                        <a id="inspiration-btn{{ $value->id }}">
                            @if($value->image != null)
                           <!-- <img src="{{ $value->getImage('medium') }}"> -->
                            @endif
                            {{ $value->getName() }}</a>
                    @endforeach
                @endif
                
            </div>
        </div>
    </div>
     @if(isset($news_categories) && count($news_categories) >0 )
        @foreach($news_categories as $value)
        @php
            $news_categories_child = $news_categories_child_collect->where('parent_id',$value->id)
        @endphp
            <div class="inspiration-cate__form" id="inspiration-form{{ $value->id }}">
                @foreach($news_categories_child as $val)
                    <a href="{{ $val->getUrl() }}">{{ $val->getName() }}</a>
                @endforeach
            </div>
        @endforeach
    @endif
   
    
    <div class="inspiration-block">
        <img src="  {!! @$config_general['image_news_2'] !!}">
        <div class="container">
            <div class="inspiration-block__list">
                <div class="item">
                    <p>  {!! @$config_general['title_news_2'] !!}</p>
                    <span>
                         {!! @$config_general['des_news_2'] !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="service-inspired">
            <div class="service-inspired__list">
                @include('web.news.item-news',['data'=>$data])
            </div>
            <div class="pagePaginate">
                {{ $data->links()??'' }}
            </div>
        </div>
    </div>
</div>
@endsection