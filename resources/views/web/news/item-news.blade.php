@if(isset($data) && count($data) )
@foreach($data as $value)
<div class="item">
    <div class="item-img">
        <a href="{{ $value->getUrl() }}">
            <img src="{{ $value->getImage('medium') }}" {{ $value->slug }}>
        </a>
    </div>
    <div class="item-infor">
        <div class="item-infor__time">
            <p>{{ date('d/m/Y H:s A',strtotime($value->created_at)) }}</p>
        </div>
        <div class="item-infor__title">
            <a href="{{ $value->getUrl() }}">
                <h3>{{ $value->getName() }}</h3>
            </a>
        </div>
        <div class="item-infor__desc">
            <p>{{$value->getDes('150')}}</p>
        </div>
    </div>
</div>
@endforeach
@endif