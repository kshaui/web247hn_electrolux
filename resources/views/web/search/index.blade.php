@extends('web.layouts.app')
@section('content')
	<div class="container">
		<div class="searchCart">
			<div class="searchCart-link">
				<p>Hiển thị kết quả tìm kiếm cho từ khóa : " {{$search}} " </p>
			</div>
			@if(count($products) > 0 || count($news) > 0)
				@if(count($products) > 0)
				<div class="searchCart-title">
					<p>Hiển thị kết quả tìm kiếm cho sản phẩm</p>
				</div>
				<div class="cateProduct-left__list">
					@include('web.products.item-product',['data'=>$products])
				</div>
				@endif

				@if(count($news) > 0)
				<div class="searchCart-title">
					<p>Hiển thị kết quả tìm kiếm cho tin tức</p>
				</div>
				<div class="service-inspired__list">
	                @include('web.news.item-news',['data'=>$news])
	            </div>
	            @endif
	        @else
	            <div class="searchCart-false">
	            	<p>Xin lỗi không có kết quả nào cho từ khóa tìm kiếm này!</p>
	            </div>
	        @endif
		</div>
	</div>  
@endsection