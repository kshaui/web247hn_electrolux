@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org",
            "@type": "Store",
            "image": "{{config('app.url')}}/public/assets/img/logo.png",
            "name": "{{config('app.name')}}",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "120 Thái Hà",
                "addressLocality": "Q. Đống Đa",
                "addressRegion": "Hà Nội",
                "postalCode": "100000",
                "addressCountry": "VN"
            },
            "priceRange": "$$",
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": 21.011915,
                "longitude": 105.821283
            },
            "telephone": "0969.120.120 - 0433.120.120"

        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>

    <link rel="stylesheet" href="/assets/libs/slick/slick.css">
    <link rel="stylesheet" href="/assets/libs/slick/slick-theme.css">
    <script src="/assets/libs/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/assets/libs/fancybox/dist/jquery.fancybox.min.css">
    <script src="/assets/libs/fancybox/dist/jquery.fancybox.min.js"></script>
@endsection
@section('content')
<div class="slider">
    @if(isset($h_slides) && count($h_slides) > 0 )
        @foreach ($h_slides as $sl)
            <div class="slider-item">
                <img src="{{ $sl->image }}">
                <div class="container">
                    <div class="item">
                        <p>{!! $sl->name !!}</p>
                        <button><a href="{!! $sl->link !!}"> Xem thêm</a> </button>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<div class="categoryHome">
    <div class="categoryHome-title">
        <div class="container">
            <button class="btn-home active" data-form=".categoryHome-list" data-hidden=".categoryHome-support">Tìm sản phẩm</button>
            <button class="btn-home" data-form=".categoryHome-support" data-hidden=".categoryHome-list">Bảo hành & Hỗ trợ</button>
        </div>
    </div>
    <div class="container">
        <div class="categoryHome-list">
            @if(isset($config_home['brands']['image']) && count($config_home['brands']['image']) > 0)
            @for($i = 0; $i < count($config_home['brands']['image']); $i++)
                <div class="categoryHome-list__item">
                    <a href="{{$config_home['brands']['text_1'][$i]}}">
                        <img src="{{$config_home['brands']['image'][$i]}}">
                        <p>{{$config_home['brands']['text_2'][$i]}}</p>
                    </a>
                </div>
            @endfor
            @endif
        </div>
        <div class="categoryHome-support">
            @foreach($config_home['conf_service']['text_1'] as $key=>$value)
                <div class="support-top__item">
                    <a href="{!! $config_home['conf_service']['text_3'][$key] !!}">
                        <div class="item-icon">
                            <span class="material-icons">
                                {!! $config_home['conf_service']['text_2'][$key] !!}
                            </span>
                        </div>
                        <div class="item-title">
                            <p>{{ $value }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="blockVideo" style="background-image: url({!! @$config_home['collection_img'] !!})">
    <div class="container">
        <div class="blockVideo-title">
            <div class="blockVideo-title__btn">
                <a href="{!! @$config_home['collection_iframe'] !!}" data-fancybox>
                    <i class="fa fa-play" aria-hidden="true"></i>
                </a>
            </div>
            <div class="blockVideo-title__item">
                <p>{!! @$config_home['collection_title'] !!}</p>
            </div>
        </div>
    </div>
</div>

<div class="blockTwo" style="background-image: url({!! @$config_home['advance_product_img'] !!})">
    <div class="container">
        <div class="blockTwo-title">
            <div class="blockTwo-title__item">
                <h3>{!! @$config_home['advance_product_title'] !!}</h3>
                <p>{!! @$config_home['advance_product_des'] !!}</p>
                <a href="{!! @$config_home['advance_product_link'] !!}">Xem thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="blockThree">
    @foreach($config_home['comprehensive_flexibility']['image'] as $val)
        <div class="blockThree-item">
            <img src="{{ $val }}">
        </div>
    @endforeach
    <div class="container">
        <div class="blockThree-list">
            @foreach($config_home['comprehensive_flexibility']['text_2'] as $key=>$val1)
                <div class="blockThree-list__text">
                    <h3>{{ $val1 }}</h3>
                    <p>{!! @$config_home['comprehensive_flexibility']['textarea'][$key] !!}</p>
                    <a href="{!! @$config_home['comprehensive_flexibility']['text_1'][$key] !!}">Xem thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="blogTitle">
    <div class="container">
        <div class="blogTitle-title">
            <p>Elextrolux blog</p>
        </div>
    </div>
</div>
<div class="blockNews">
    <div class="blockNews-item">
        <img src="{!! @$config_home['news_home_img'] !!}">
    </div>
    <div class="blockNews-item">
         <img src="{!! @$config_home['news_home_img_1'] !!}">
    </div>
    <div class="container">
        <div class="blockNews-btn">
            <a href="{!! @$config_home['news_home_video'] !!}" data-fancybox>
                <i class="fa fa-play" aria-hidden="true"></i>
            </a>
        </div>
        <div class="blockNews-list">
            <div class="blockNews-list__text">
                <p>{!! @$config_home['news_home_des'] !!}</p>
            </div>
            <div class="blockNews-list__text">
                <a href="{!! @$config_home['news_home_link_1'] !!}">Xem thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <p>{!! @$config_home['news_home_des_1'] !!}</p>
            </div>
        </div>
    </div>
</div>
<div class="formHome" style="background-image: url(assets/img/product/2714869.jpg)">
    <div class="container">
        <div class="blogTitle">
            <div class="container">
                <div class="blogTitle-title">
                    <p>Đăng kí nhận thông tin sản phẩm và các trương trình khuyến mãi</p>
                </div>
            </div>
        </div>
        <div class="formHome-list">
            <form>
                @csrf
                <input type="text" class="name_contacts_home" placeholder="*Tên">
                <input type="text" class="email_contacts_home" placeholder="*Email">
                <input type="text" class="phone_contacts_home" placeholder="*Số điện thoại">
                <button type="button" onclick="send_contact()" class="btn-login">Đăng ký ngay <i class="fa fa-angle-right" aria-hidden="true"></i></button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('foot')
    <script type="text/javascript" src="/assets/js/slick.js"></script>
@endsection