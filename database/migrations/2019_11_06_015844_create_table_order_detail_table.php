<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable();
            $table->string('quanity',191)->nullable();
            $table->string('price',191)->nullable();
            $table->string('price_total',191)->nullable();
            $table->string('order_id',191)->nullable();
            $table->string('product_id',191)->nullable();
            $table->string('type',191)->nullable();
            $table->unique('id','id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail');
    }
}
