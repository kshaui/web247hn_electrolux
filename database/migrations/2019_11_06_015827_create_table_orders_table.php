<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable();
            $table->string('name',191)->nullable();
            $table->string('email',191)->nullable();
            $table->string('adress',191)->nullable();
            $table->string('phone',191)->nullable();
            $table->string('provincial',191)->nullable();
            $table->string('district',191)->nullable();
            $table->string('note',191)->nullable();
            $table->string('payment_method',191)->nullable();
            $table->string('total_price')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->unique('id','id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
