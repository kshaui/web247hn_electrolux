<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191);
            $table->string('slug',191);
        
            $table->string('image',191)->nullable();
     
            $table->text('description')->nullable();
            $table->longText('detail')->nullable();
            $table->string('related_products',191)->nullable();
            $table->string('related_news',191)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
